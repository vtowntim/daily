if (typeof console != "undefined") console.clear();

throttleMinutes = 0;

apiTools = {};

apiTools.cacheNameFromObj = function(object){
	return JSON.stringify(object).replace(/\W|(from)|where|select|order|like|ds|by|mode|e/gi,'');
}

apiTools.cacheSet = function(itemName, value){
	itemName = typeof(itemName) != 'string' ? apiTools.cacheNameFromObj(itemName) : itemName;

	if (typeof(Storage) === "undefined") return false;

	localStorage.setItem(itemName, JSON.stringify(value));
	localStorage.setItem(itemName+'_TIME', moment().toJSON());

	if (typeof console != "undefined") console.log(itemName + ' Saved to Cache');
}

apiTools.cacheGet = function(itemName, throttleMins){
	itemName = typeof(itemName) != 'string' ? apiTools.cacheNameFromObj(itemName) : itemName;
	if (typeof throttleMins == 'undefined') throttleMins = 10;

	if (typeof(Storage) === "undefined") return false;

	var item = localStorage.getItem(itemName);
	if (!item || !item) return false;

	var itemTime = localStorage.getItem(itemName+'_TIME');
	if (moment().diff(itemTime,'minutes') >= throttleMins ) return false;

	if (typeof console != "undefined") console.log(itemName + ' Received from Cache -- Age: ' + moment(itemTime).fromNow(1) );

	if (item == 'undefined') return false;

	return JSON.parse(item);
}

formatPhone = function(number){
	if (number === 'undefined') return number;
	number = $.trim(number)
    	.replace(/(\(?(559)\)?\s?)?624-?/gi,'')
    	.replace(/\(559\)\s?/gi,'')
    	.replace(/^9([0-9]{7})$/gi,'$1')
    	.replace(/^559([0-9]{7})$/gi,'$1')
    	.replace(/([0-9]{3})([0-9]{4})/gi,'$1-$2');
    if (number.length == 4) number = '(559) 624-' + number;
	return number;
}

function makeMap(myLocation,size){
	size = size || '300x400';
	var zoom = size == '75x75' ? '15' : '16';
	locations = {
		'The LifeStyle Center': '5105 W Cypress Ave Visalia, CA',
		'Kaweah Delta Medical Center': '400 W Mineral King Ave Visalia, CA',
		'Support Services Building': '520 W Mineral King Ave',
		'Hospice': '900 W Oak Ave, Visalia, CA',
		'Kaweah Delta Mental Hlth Hosp.': '1100 S Akers St, Visalia, CA',
		'Kaweah Delta Rehabilitation': '840 S Akers St, Visalia, CA',
		'Sequoia Imaging Center': '4949 W Cypress Ave, Visalia, CA',
		'Sequoia Regional Cancer Care': '4945 W Cypress Ave, Visalia, CA',
		'Sequoia Prompt Care': '1110 S Ben Maddox Way, Visalia, CA',
		'Exeter Health Clinic': '1014 San Juan Ave Exeter, CA',
		'Lindsay Health Clinic': '839 North Sequoia Avenue Lindsay, CA',
		'Woodlake Health Clinic': '180 E. Antelope Woodlake, CA',
		'Prime Infusion Network': '602 W Willow St, Visalia, CA'
	};

	if (locations[myLocation]){
		var loc = escape(locations[myLocation]);
	}else{
		var loc = escape(locations['Kaweah Delta Medical Center']);
	}
	var marker = size == '75x75' ? '' : '&markers=color:red|'+loc ;
	return 'http://maps.googleapis.com/maps/api/staticmap?center='+ loc +'&zoom='+zoom+'&size='+size+'&sensor=false&visual_refresh=true' + marker;
};


var apiURL = 'db/api.aspx?callback=?';



var vm = {
	appState: ko.observable(),
	loadCount: ko.observable(0),
	user: ko.observable({
		username: ko.observable(cookie.get('username')),
		userFullName: ko.observable(cookie.get('userFullName')),
		password: ko.observable(),
		status: ko.observable(false),
		groups: ko.observable(cookie.get('groups'))
	}),
	seltdContact: ko.observable({
		studentID: ko.observable(),
		LName: ko.observable(),
		FName: ko.observable(),
		SDate: ko.observable(),
		EDate: ko.observable(),
		School: ko.observable(),
		STypeID: ko.observable(),
		SpecID: ko.observable()
	}),
	student: {
		specialtyTypes: ko.observable(),
		studentTypes: ko.observable()
	},
	resident: {
		rotations: ko.observable()
	},
	seltdType: ko.observable(),
	search: ko.observable(),
	lastSearch: ko.observable(),
	hashHistory: ko.observableArray(),
	loadingContacts: ko.observableArray(),
	contacts: ko.observableArray([]),
	zones: {
		ISS: ko.observable(false),
		editStudents: ko.observable(false),
		editResidents: ko.observable(false)
	}
}


ko.bindingHandlers.searchResultHighlight = {
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var valueUnwrapped = ko.unwrap(valueAccessor());
        var searchText = vm.search();
        var rex = new RegExp('('+searchText+')', 'gi');
        var name;
        if (valueUnwrapped.l){
        	name = valueUnwrapped.f + ' <strong>'+ valueUnwrapped.l +'</strong>';
        }else{
        	name = valueUnwrapped.t;
        }
        if (searchText && searchText.length > 1) name = name.replace(rex, '<span class="highlight">$1</span>');
        $(element).html(name);
    }
};



ko.bindingHandlers.contactPhone = {
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var valueUnwrapped = ko.unwrap(valueAccessor());
        var myPhone = valueUnwrapped.phone ? valueUnwrapped.phone : valueUnwrapped.deptPhone;
        myPhone = formatPhone(myPhone);
        $(element).html(myPhone);
    }
};

ko.bindingHandlers.kdPeople = {
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var valueUnwrapped = ko.unwrap(valueAccessor());
        var navObj = {
        	verb: 'search',
        	adj: 'all',
        	noun: 'a',
        	caption: 'A'
        };

        _.extend(navObj, valueUnwrapped);
        
        $(element).html('<a href="#'+ navObj.verb +'/'+ navObj.adj +'/'+ encodeURI(navObj.noun) +'">'+ navObj.caption +'</a>');
    }
};

ko.punches.interpolationMarkup.enable();
ko.punches.textFilter.enableForBinding("text");

ko.applyBindings(vm);

//	Sort by FName:
//  vm.contacts(_.sortBy(vm.contacts(), 'FName'))

getID = function(data){
	switch(data.type){
		case 'physician':
			return data.mdNum.split('/')[0];
		break;
		default:
			return data.id;
	}
}

changeType = function(type){
	type = hashToNavObj().adj == type ? null : type;
	if (type){
		navObjToHash({verb:'search',adj: type, noun:null});
	}else{
		navObjToHash(null);
	}
	$('#search').focus();
}

viewLoginBox = function(){
	navObjToHash({verb:'login'});
	setTimeout(function(){
		$('#username').focus();
	},5);
}



permCheck = function(adGroup){
	var ckRegExp = new RegExp('('+ adGroup +'.*?),','g');
	return vm.user().groups().match(ckRegExp) ? true : false;
}

processPermissions = function(){
	if (permCheck('KDPeople Residents Edit')) vm.zones.editResidents(true);
	if (permCheck('KDPeople Students Edit')) vm.zones.editStudents(true);
	if (permCheck('Information Systems Services')) vm.zones.ISS(true);
}

retrieveData = function(dataObj, callback){

	// var cached = apiTools.cacheGet(dataObj, throttleMinutes);
	// if (cached){
	// 	if (callback) callback(cached);
	// 	return false;
	// }

	vm.loadCount( vm.loadCount() + 1 );

	$.getJSON(apiURL, dataObj,
		function(data){
			vm.loadCount( vm.loadCount() - 1 );
			if (callback) callback(data);
			//apiTools.cacheSet(dataObj, data);
		});

}

addToResults = function(newData){
	var myArray = vm.loadingContacts();
	ko.utils.arrayPushAll(myArray, newData);
	vm.loadingContacts.sort(function(left, right) {
		if (left.lName == hashToNavObj().noun) return -1;
		return left.lName == right.lName ? 0 : (left.lName < right.lName ? -1 : 1)
	})
	if (vm.loadCount() == 0){
		vm.contacts(vm.loadingContacts());
	};
}

letter = function(myLetter){
	vm.search(myLetter);
	doSearch();
	$('#search').select();
}

doSearch = function(){
	//console.log('search');
	navObjToHash({verb:'search', noun: vm.search()});
	//$('#search').select();
}

throttledSearch = _.debounce(doSearch, 1000);



getContacts = function(type, term){

	//If we're re-doing the same search and the user isn't logged in, just keep the last results
	if (vm.lastSearch() == type + term && !vm.user().status()) return false;
	vm.lastSearch(type + term);

	vm.contacts([]); vm.loadingContacts([]);

	type = type == 'all' ? null : type;

	//if the type is in the array, the default search term is % otherwise its A
	var defaultTerm = ['department','resident','student'].indexOf(type) + 1 ? '%' : 'A'

	term = term || defaultTerm;
	
	var where = '', deptWhere = '';
	switch(term.length){
		case 0:
			return false
		break;
		case 1:
			where = "lName like '" +term + "%'";
			deptWhere = "DeptName like '"+ term +"%' "; //or Alt_Names like '"+ term.replace(/\s/g,'%') +"%' or Alt_Names like '%,"+ term.replace(/\s/g,'%') +"%' ";
		break;
		default:
			where = "fullName like '%" +term.replace(/\s/g,'%') + "%' or username like '%" +term.replace(/\s/g,'%') + "%'  or lName + ' ' + fName + ' ' + lName like '%" +term.replace(/\s/g,'%') + "%' or dept like '%" +term + "%'";
			deptWhere = "DeptName like '%"+ term.replace(/\s/g,'%') +"%' or location like '%"+ term.replace(/\s/g,'%') +"%' or id = \'" + term.replace(/\s/g,'%') + "\' " //or Alt_Names like '%"+ term.replace(/\s/g,'%') +"%'";
	}

	if (term.match(/^[0-9]{3,5}$/)){
	// //	vm.searchType('Doctor');
	 	where = "mdNum like '"+ term.replace(/^00/,'') +"%'";
	 }

	if (term.match(/^\[.*\]$/)){
	//	vm.searchType('Department');
		where = "DeptName = '"+ term.replace(/\[|\]/g,'') +"%'";
	}


	if (!type || type == 'department'){
		var dataObj = {
				mode:'select',
				from: 'employee.View_dir_deptartment',
				where: "where "+ deptWhere +" order by DeptName"	
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}


	
		var dataObj = {
				mode:'search',
				sp: 'resident.sp_view_dr_search',
				term: term.replace(/\s/g,'%'),
				flag: term.length == 1 ? 1 : 0
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});

		var dataObj = {
				mode:'search',
				sp: 'physician.sp_view_dr_search',
				term: term.replace(/\s/g,'%'),
				flag: term.length == 1 ? 1 : 0
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});



}

getRotations = function(){

	retrieveData({
		mode: 'select',
		from: '[resident].[Tbl_Rotation_List]'
	},function(data){
		var sortedData = _.sortBy(data, function(e){return e.type;});
		vm.resident.rotations(sortedData);
	})

}

getSpecialties = function(){

	retrieveData({
		mode: 'select',
		from: '[student].[Tbl_Specialty_List]'
	},function(data){
		var sortedData = _.sortBy(data, function(e){return e.specialty;});
		vm.student.specialtyTypes(sortedData);
	})

}

getStudentTypes = function(){

	retrieveData({
		mode: 'select',
		from: '[student].[Tbl_StudentType_List]'
	},function(data){
		var sortedData = _.sortBy(data, function(e){return e.specialty;});
		vm.student.studentTypes(sortedData);
	})

}

loadCallPrefs = function(drNum, callback){
	retrieveData({
		mode:'callPrefs',
		doctor_number:drNum
	},function(data){
		var culledData = [];
		$.each(data,function(k,v){
			culledData.push({
				order: data[k].Call_Order,
				type: data[k].Call_Type,
				details: data[k].Call_Details,
				number: data[k][data[k].Call_Field_Name]
			});
		})
		if(callback)callback(culledData);
	})
}

loadContact = function(navObj, callback){
	var id = navObj.noun;
	var type = navObj.adj;

	var payload;

	//console.log(type);

	switch(type){
		case 'student':
			payload = {
				mode: 'select',
				from: 'student.View_dir',
				where: 'where id = ' + id
			}
		break;
		case 'resident':
			payload = {
				mode: 'select',
				from: 'resident.View_dir',
				where: 'where id = ' + id
			}
		break;
		case 'employee':
			payload = {
				mode: 'select',
				from: 'employee.View_dir',
				where: 'where [id] = \'' + id + '\''
			}
		break;
		case 'department':
			payload = {
				mode: 'select',
				from: 'employee.View_dir_deptartment',
				where: 'where [id] = ' + id
			}
		break;
		case 'physician':
			payload = {
				mode: 'select',
				from: 'physician.View_dir',
				where: 'where mdNum like \'' + id + '%\''
			}
		break;
		default:
			return false;
	}

	if(id=='new'){
		payload.where = '';
		payload.select = 'top 1 *';
		callback({
			id: null,
			type: type,
			lName: null,
			fName: null,
			mName: null,
			fullName: null,
			dept: null,
			phone: null,
			deptPhone: null,
			username: null,
			SDate: null,
			EDate: null,
			School: null,
			STypeID: null,
			SpecID: null
		});
		return false;
	}

	$.getJSON(apiURL, payload, function(data){
		data = data[0];
		if(id=='new'){
			$.each(data,function(k,v){
				data[k] = null;
			})
			data.type = type;
		}
		if (data.SDate) data.SDate = moment(data.SDate).format('YYYY-MM-DD');
		if (data.EDate) data.EDate = moment(data.EDate).format('YYYY-MM-DD');
		if (callback) callback(data);
	});
}

openContact = function(contact){
	vm.seltdContact( ko.mapping.fromJS( contact ) );
	delete vm.seltdContact().__ko_mapping__;
}

newPerson = function(type){
	location.hash = 'edit/'+ type +'/new';
}

removeFalsyProperties = function(myObject){
	$.each(myObject, function(k,v){
		if (!v){
			delete myObject[k];
		}
	});
	return myObject;
}


prepForDB = function(myString, joinLeft, joinRight){
	if (typeof myString == 'object') myString = joinLeft + myString.join( joinRight + ',' + joinLeft ) + joinRight;
	myString = myString.replace(/\'(undefined)?\'/g,'null');
	return myString
}

objToParameters = function(myObject){
	var parameters = [];
	$.each(myObject, function(k,v){
		parameters.push(k + '=' + '\'' + v + '\'');
	});
	return parameters.join(',')
}



clearSeltdContact = function(){
	for(var index in vm.seltdContact() ) {
	   if(ko.isObservable(vm.seltdContact()[index])) {
	      vm.seltdContact()[index](null);
	   }
	}
}


backToSearch = function(){
	var navObj = hashToNavObj();

	if(navObj.verb == 'edit'){
		if (!confirm('Are you sure you want to close this contact without saving?')) return false;
	}

	// navObjToHash({
	// 	verb: 'search',
	// 	adj: hashToNavObj().adj,
	// 	noun: vm.search()
	// });

	returnToSearch();

	
}

returnToSearch = function(){
	navObjToHash(
		_.last(_.where(vm.hashHistory(), {verb: 'search'}))
		);
}

clearSearch = function(){
	navObjToHash({});
	$('#search').val('').select();
}

navObjToHash = function(navObj){

	navObj = navObj || {};

	if ( ! _.isEmpty(navObj) ){
		var currentNavObj = hashToNavObj();
		_.extend(currentNavObj, navObj);
		navObj = currentNavObj;
	}

	if (navObj.noun && !navObj.adj){
		navObj.adj = 'all';
	}

	var myHash = _.toArray(navObj).join('/');
	location.hash = myHash;
}

objArrayToCSV = function(objArray){
	var workingArray = [];

	workingArray.push( '"' + _.keys(objArray[0]).join('","') + '"' );

	$.each(objArray, function(k,v){
		workingArray.push( '"' + _.values(v).join('","') + '"' )
	})

	return workingArray.join('\r\n');
}

openCSV = function(csvData){
	var DataURI = 'data:text/csv;base64,' + btoa( objArrayToCSV(csvData) );
	//location.href =  DataURI;
	//$('<a>#</a>').attr('href', DataURI).attr('download','KDPeople.csv').appendTo('body').click();

	var navObj = hashToNavObj();

	var link = document.createElement('a');
    link.download = 'KDPeople - '+ navObj.adj +' - '+ navObj.noun +'.csv';
    link.href = DataURI;
    link.click();
}

downloadContacts = function(){
	openCSV(vm.contacts());
}

hashToNavObj = function(hash){
	hash = hash || location.hash;
	var hashArr = hash.match(/[^#/]*/g); //Break the sections into an array
	var navObj = _.object(['verb','adj','noun'], _.compact(hashArr)); //Convert to an object by mapping the parameter names to the array
	if (navObj.noun) {
		navObj.noun = decodeURI(navObj.noun);
		}
	return navObj;
}




$(window).hashchange( function(){

	var navObj = hashToNavObj();
	vm.hashHistory.push(navObj);

	if (!navObj.adj) vm.seltdType(null);

	switch(navObj.verb){
		case 'login':
			vm.appState('login');
		break;
		case 'edit':
			if (navObj.noun){
				loadContact(navObj, function(contact){
					openContact(contact);
					vm.appState('edit');
				})
			}
		break;
		case 'view':
			if (navObj.noun){

				loadContact(navObj, function(contact){
					

					if (contact.type == 'physician'){
						vm.seltdContact().callPrefs = [];
						loadCallPrefs(contact.id,function(callPrefs){
							vm.seltdContact().callPrefs = callPrefs;
							vm.seltdContact.valueHasMutated();
						})
					}

					openContact(contact);

					vm.appState('view');
				});

			}

		break;
		case 'search':
			vm.appState('');

			if (navObj.adj){
				if (navObj.noun || ['department','employee','resident','student','physician'].indexOf(navObj.adj) + 1 ){
					if (navObj.noun != 'all') vm.search(navObj.noun);
					vm.seltdType(navObj.adj);
					getContacts(navObj.adj, navObj.noun);
				}else{
					navObjToHash({adj: 'all', noun: navObj.adj});
				}
			}else{
				vm.seltdType(null);
				vm.contacts(null);
				$('#search').focus();
				//getContacts('all','');
			}

		break;

		default:
			vm.appState('');
			vm.contacts(null);
	}

})


$('#contacts').on('click', 'li', function(){
	navObjToHash({
		verb: 'view',
		adj: $(this).attr('data-type'),
		noun: $(this).attr('data-id')
	});
});


$('#search').keyup(function(e){
	$(this).change();
	throttledSearch();
	if (e.which == 13){
		doSearch();
	}
})



$(function(){


	getRotations();
	getSpecialties();
	getStudentTypes();

	var navObj = hashToNavObj();
	if (navObj.verb == 'search'){
		vm.seltdType(navObj.adj);
	}
	

	$(window).hashchange();

})



