console.clear();

apiTools = {};

apiTools.cacheNameFromObj = function(object){
	return JSON.stringify(object).replace(/\W|(from)|where|select|order|like|ds|by|mode|e/gi,'');
}

apiTools.cacheSet = function(itemName, value){
	itemName = typeof(itemName) != 'string' ? apiTools.cacheNameFromObj(itemName) : itemName;

	if (typeof(Storage) === "undefined") return false;

	localStorage.setItem(itemName, JSON.stringify(value));
	localStorage.setItem(itemName+'_TIME', moment().toJSON());

	if (typeof console != "undefined") console.log(itemName + ' Saved to Cache');
}

apiTools.cacheGet = function(itemName, throttleMins){
	itemName = typeof(itemName) != 'string' ? apiTools.cacheNameFromObj(itemName) : itemName;
	throttleMins = throttleMins || 10;

	if (typeof(Storage) === "undefined") return false;
	
	var item = localStorage.getItem(itemName);
	if (!item || !item) return false;
	
	var itemTime = localStorage.getItem(itemName+'_TIME');
	if (moment().diff(itemTime,'minutes') >= throttleMins ) return false;
	
	if (typeof console != "undefined") console.log(itemName + ' Received from Cache -- Age: ' + moment(itemTime).fromNow(1) );
	
	if (item == 'undefined') return false;

	return JSON.parse(item);
}


var apiURL = 'db/api.aspx?callback=?';



var vm = {
	appState: ko.observable(),
	loadCount: ko.observable(0),
	user: ko.observable({
		username: ko.observable(cookie.get('username')),
		password: ko.observable(),
		status: ko.observable(false)
	}),
	seltdContact: ko.observable({
		studentID: ko.observable(),
		LName: ko.observable(),
		FName: ko.observable(),
		SDate: ko.observable(),
		EDate: ko.observable(),
		School: ko.observable(),
		STypeID: ko.observable(),
		SpecID: ko.observable()
	}),
	student: {
		specialtyTypes: ko.observable(),
		studentTypes: ko.observable()
	},
	seltdType: ko.observable(),
	search: ko.observable(),
	contacts: ko.observableArray()
}

ko.bindingProvider.instance = new StringInterpolatingBindingProvider();
ko.applyBindings(vm);

//	Sort by FName:
//  vm.contacts(_.sortBy(vm.contacts(), 'FName'))

getID = function(data){
	switch(data.type){
		case 'Doctor':
			return data.mdNum;
		break;
		default:
			return data.id;
	}
}

changeType = function(type){
	type = hashToNavObj().adj == type ? '' : type;
	navObjToHash({verb:'search',adj: type});
}

viewLoginBox = function(){
	navObjToHash({verb:'login'});
	setTimeout(function(){
		$('#username').focus();
	},5);
}

login = function(){
	vm.user().status('Logging In');
	$.getJSON(apiURL, {
		mode: 'login',
		username: vm.user().username(),
		password: vm.user().password()
	}, function(data){
		if (data[0].auth) { loggedIn(); }
	})
}

loggedIn = function(doNotSetCookie){
	vm.user().status('Logged In');
	vm.user().password(null);

	if (!doNotSetCookie){
		cookie.set('username', vm.user().username(), {
		   expires: moment().add('minutes', 90).toDate(), // expires in 90 minutes
		});
		navObjToHash({});
	}

}

logOut = function(){
	cookie.empty();
	vm.user().username(null);
	vm.user().status(null);
	navObjToHash({});
}

retrieveData = function(dataObj, callback){

	var cached = apiTools.cacheGet(dataObj, 60);
	if (cached){
		if (callback) callback(cached);
		return false;
	}

	vm.loadCount( vm.loadCount() + 1 );

	$.getJSON(apiURL, dataObj,
		function(data){
			vm.loadCount( vm.loadCount() - 1 );
			if (callback) callback(data);
			apiTools.cacheSet(dataObj, data);
		});

}

addToResults = function(newData){
	var myArray = vm.contacts();
	ko.utils.arrayPushAll(myArray, newData);
	vm.contacts.sort(function(left, right) {
		return left.lName == right.lName ? 0 : (left.lName < right.lName ? -1 : 1)
	})
	vm.contacts.valueHasMutated();
}

letter = function(myLetter){	
	vm.search(myLetter);
	doSearch();
	$('#search').select();
}

doSearch = function(){
	navObjToHash({verb:'search', noun: vm.search()});
	$('#search').select();
}

getContacts = function(type, term){
	vm.contacts([]);

	type = type == 'All' ? null : type;

	term = term || 'A';
	
	var where = '', deptWhere = '';
	switch(term.length){
		case 0:
			return false
		break;
		case 1:
			where = "lName like '" +term + "%'";
			deptWhere = "DeptName like '"+ term +"%' "; //or Alt_Names like '"+ term.replace(/\s/g,'%') +"%' or Alt_Names like '%,"+ term.replace(/\s/g,'%') +"%' ";
		break;
		default:
			where = "fullName like '%" +term.replace(/\s/g,'%') + "%' or username like '%" +term.replace(/\s/g,'%') + "%'  or fName + ' ' + lName like '%" +term.replace(/\s/g,'%') + "%' or dept like '%" +term + "%'";
			deptWhere = "DeptName like '%"+ term.replace(/\s/g,'%') +"%' or Alt_Names like '%"+ term.replace(/\s/g,'%') +"%'";
	}

	if (term.match(/^[0-9]{3,5}$/)){
	//	vm.searchType('Doctor');
		where = "mdNum like '"+ term.replace(/^00/,'') +"%'";
	}

	if (term.match(/^\[.*\]$/)){
	//	vm.searchType('Department');
		where = "DeptName = '"+ term.replace(/\[|\]/g,'') +"%'";
	}

	if (!type || type == 'Student'){
		var dataObj = {
			mode: 'select',
			from: 'student.View_dir',
			select: '*',
			where: "where "+ where +" order by lName, fName"
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}

	if (!type || type == 'Resident'){
		var dataObj = {
			mode: 'select',
			from: 'resident.View_dir',
			select: '*',
			where: "where "+ where +" order by lName, fName"
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}

	if (!type || type == 'Department'){
		var dataObj = {
				ds: 'EmpDir',
				mode:'select',
				select: '*, DeptName as [fName], \'\' as [lName], DeptID as [id], \'Department\' as [type] ',
				from: 'Departments',
				where: "where "+ deptWhere +" order by DeptName"	
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}

	if (!type || type == 'Employee'){
		var dataObj = {
				ds: 'EmpDir',
				mode:'select',
				select: '*',
				from: 'View_dir',
				where: "where "+ where +" order by lName, fName"
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}

	if (!type || type == 'Physician'){
		var dataObj = {
				ds: 'PhyDir',
				mode:'select',
				select: '*',
				from: 'View_dir',
				where: "where "+ where +" order by lName, fName"
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}

	
}

getSpecialties = function(){
	$.getJSON(apiURL,{
		mode: 'select',
		from: '[student].[Tbl_Specialty_List]'
	},function(data){
		var sortedData = _.sortBy(data, function(e){return e.specialty;});
		vm.student.specialtyTypes(sortedData);
	})
}

getStudentTypes = function(){
	$.getJSON(apiURL,{
		mode: 'select',
		from: '[student].[Tbl_StudentType_List]'
	},function(data){
		var sortedData = _.sortBy(data, function(e){return e.specialty;});
		vm.student.studentTypes(sortedData);
	})
}

loadContact = function(navObj, callback){
	var id = navObj.noun;
	var type = navObj.adj;

	if(id=='new'){
		clearSeltdContact();
		if (callback){callback};
		return false;
	}

	var data;

	switch(type){
		case 'Student':
			data = {
				mode: 'select',
				from: 'student.View_dir',
				where: 'where id = ' + id
			}
		break;
		case 'Resident':
			data = {
				mode: 'select',
				from: 'resident.View_dir',
				where: 'where id = ' + id
			}
		break;
		case 'Employee':
			data = {
				mode: 'select',
				ds: 'EmpDir',
				from: 'View_dir',
				where: 'where id = ' + id
			}
		break;
		case 'Doctor':
			data = {
				mode: 'select',
				ds: 'PhyDir',
				from: 'View_dir',
				where: 'where mdNum = \'' + id + '\''
			}
		break;
		default:
			return false;
	}

	retrieveData(data, function(data){
		if (callback) callback(data[0]);
	});
}

openContact = function(contact){
	vm.seltdContact( ko.mapping.fromJS( contact ) );
	delete vm.seltdContact().__ko_mapping__;
}

newStudent = function(){
	location.hash = 'edit/student/new';
}

removeFalsyProperties = function(myObject){
	$.each(myObject, function(k,v){
		if (!v){
			delete myObject[k];
		}
	});
	return myObject;
}


prepForDB = function(myString, joinLeft, joinRight){
	if (typeof myString == 'object') myString = joinLeft + myString.join( joinRight + ',' + joinLeft ) + joinRight;
	myString = myString.replace(/\'\'/g,'null');
	return myString
}

objToParameters = function(myObject){
	var parameters = [];
	$.each(myObject, function(k,v){
		parameters.push(k + '=' + '\'' + v + '\'');
	});
	return parameters.join(',')
}

saveContact = function(){
	var myData = ko.toJS(vm.seltdContact());
	var payload;

	if (!myData.studentID) { 
		delete myData.studentID; 
		payload = {
			mode: 'insert',
			table: 'student.Masterdata',
			columns: prepForDB(_.keys(myData), '[', ']'),
			values: prepForDB(_.values(myData), '\'', '\'' )
		};
	}else{
		var studentID = myData.studentID;
		delete myData.studentID;

		payload = {
			mode: 'update',
			table: 'student.Masterdata',
			values: prepForDB(objToParameters(myData)),
			where: 'WHERE studentID = ' + studentID
		};
	}

	$.getJSON(apiURL, payload, function(data){
		console.log(data);
		var myID = studentID ? studentID : data.id;
		uploadPhoto(myID,'student.Slave_Image','studentID, studentimage');
		navObjToHash({verb: 'view', noun: myID});
	})
}

selectPhoto = function(){
	$('#newPhoto').click();
}

selectedPhoto = function(me){
	var file = me.files[0];
    if (!file || !(file.type.indexOf('image/') + 1)) {
    	$('#profilePhoto').css({'background-image': 'url(http://apps.kdhcd.org/dir/new/img/noPhoto.jpg)'});
    	return false;
    };

    fr = new FileReader();
	fr.readAsDataURL(file);
	fr.onload = function(){
		$('#profilePhoto').css({'background-image': 'url('+ fr.result + ')' });
		};

}

uploadPhoto = function(id, table, columns){
	if (!$('#newPhoto').val()) { return false; }

	$('#newPhoto').upload(apiURL,{
		mode: 'upload',
		table: table,
		columns: columns,
		id: id
	}, function(){ });
}

clearSeltdContact = function(){
	for(var index in vm.seltdContact() ) {
	   if(ko.isObservable(vm.seltdContact()[index])) {
	      vm.seltdContact()[index](null);
	   }
	}
}

editContact = function(){
	navObjToHash({verb: 'edit'});
}

backToSearch = function(){
	navObjToHash({
		verb: 'search',
		adj: vm.seltdType(),
		noun: vm.search()
	});
}

navObjToHash = function(navObj){


	if ( ! _.isEmpty(navObj) ){
		var currentNavObj = hashToNavObj();
		_.extend(currentNavObj, navObj);
		navObj = currentNavObj;
	}

	if (navObj.noun && !navObj.adj){
		navObj.adj = 'All';
	}

	var myHash = _.toArray(navObj).join('/');
	location.hash = myHash;
}


hashToNavObj = function(hash){
	hash = hash || location.hash;
	var hashArr = hash.match(/[^#/]*/g); //Break the sections into an array
	return _.object(['verb','adj','noun'], _.compact(hashArr)); //Convert to an object by mapping the parameter names to the array
}




$(window).hashchange( function(){

	var navObj = hashToNavObj();

	switch(navObj.verb){
		case 'login':
			vm.appState('login');
		break;
		case 'edit':
			if (navObj.noun){
				loadContact(navObj, function(contact){
					openContact(contact);
					vm.appState('edit');
				})
			}
		break;
		case 'view':
			if (navObj.noun){

				loadContact(navObj, function(contact){
					openContact(contact);
					vm.appState('view');
				});

			}

		break;
		case 'search':
			vm.appState('');

			if (navObj.adj){
				vm.seltdType(navObj.adj);
				getContacts(navObj.adj, navObj.noun);
			}

		break;

		default:
			vm.appState('');
	}

})


$('#contacts').on('click', 'li', function(){
	navObjToHash({
		verb: 'view',
		adj: $(this).attr('data-type'),
		noun: $(this).attr('data-id')
	});
});


$('#search').keyup(function(e){
	if (e.which == 13){
		doSearch();
	}
}).change(function(e){
	doSearch();
})



$(function(){

	if (cookie.get('username')){
		loggedIn(true);
	}

	getSpecialties();
	getStudentTypes();

	$(window).hashchange();

})





