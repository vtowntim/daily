#Standards of Care:
#Adult Medical/Surgical Units
​
The following Standards of Care are meant to provide the Registered Nurse (RN) with minimum guidelines for providing routine care.  Professional judgment, physician’s orders and/or a change in the patient’s status can escalate the frequency of systematic assessments, reassessments and interventions. The RN may delegate appropriate interventions to assistive personnel if within their scope of practice. The patient, family or other legal representative shall be included in the development and implementation of the patient’s plan of care.

*"These guidelines do not represent the only medically or legally acceptable approach, but rather are presented with the recognition that acceptable approaches exist.  Deviations under appropriate circumstances do not represent a breach of a standard of care.  New knowledge, new techniques, clinical or research data, clinical experience, or clinical or bio-ethical circumstances may provide sound reasons for alternative approaches, even though they are not described in the document." Kaweah Delta Health Care District RN Standards of Care: ICU/CVICU (03,2012)*

####Approved nursing resources available on the unit:
- **Clinical Nursing Skills and Techniques** (latest edition) by Perry and Potter
- Kaweah Delta Health Care District policies and procedures.

## BASIC PHYSIOLOGICAL, NEEDS & SAFETY ASSESSMENTS
A basic head to toe assessment/reassessment and development of the plan of care should be conducted on admission, every shift, and with full assumption of care (excluding lunch coverage) to identify immediate and/or emerging needs, change in patient condition, and to determine response to treatment/procedures unless indicated otherwise. Abnormal findings are communicated to the appropriate provider.

### PHYSIOLOGICAL ASSESSMENT

####1. Early Warning Recognition

The RN is responsible for assessment and surveillance of:

- Reassess changes in patient condition and/or vital signs utilizing basic head-to-toe assessment and 10 Signs of Vitality (early recognition criteria).
    - Temperature < 36°C or ≥ 38°C
    - Pulse < 50 or >100 beats per minute
    - Pain: new or significantly increased
    - Respiratory rate < 6 or > 20 breaths per minute
    - SaO2 < 90% and/or increasing O2 requirements
    - Systolic blood pressure < 90 mmHg or Mean arterial pressure < 60 mmHg
    - Change in level of consciousness, such as increased anxiety and/or lethargy.
    - Urine output < 30 ml /hour or 100 ml/ 4 hours (excluding renal failure)
    - Delayed capillary refill (> 3-seconds) and or mottled skin
    - Lab results: Lab Results: Base excess <-5 or Lactic Acid >
- Location of emergency/code blue cart and supplies.
- Procedure for activating Code Blue and RRT alerts.
- Emergency supplies and equipment functioning properly ( e.g. suction set up, yankauer, suction tips, masks, ambu bag with O2).
- The RN is responsible for interventions:
- Activate Rapid Response Team (RRT) alert or code blue when indicated then ensures notification of lead nurse, house supervisor, primary and/or other provider.
- Ensure emergency equipment, supplies and medications are brought to the bedside.
- Perform BLS and defibrillation for VFib or pulseless/unconscious VTach. Reference policy [CO.01 Code Blue/White](kdnet.kdhcd.org/policy/policy_docs/typeid_03/code%20blue%20white%20co.01%205-14-12.doc).

####2. ​Neurological/HEENT

The RN is responsible for assessment and surveillance of:

- Basic neurological assessment: Level of consciousness, orientation (person, place, time) and behaviors (appropriate, agitated, combative); pupils and facial symmetry if indicated; limb movement, strength and change in sensation; speech/ability to phonate and use of language.
- Sleep preferences, frequency and duration.
- The RN is responsible for interventions:
- Orient/reorient to date, time, place, event, and environment.
- Coordinate procedures, providing comfort measures, offering sleep interventions, and minimizing noise in and around patient room.
- Promote care to facilitate normal routines and preserve functional and cognitive status (e.g.  Use of sensory aids, daily bathing, activities of daily living (ADLs), medication schedules, mobilization, procedures during the day shift).

####3. Cardiovascular

The RN is responsible for assessment and surveillance of:

- Apical pulse: Ascultate
- Pulses (radial, dorsalis pedis and posterior tibial):  Palpate
- Capillary refill.
- Skin color, temperature, moisture, and edema.
- Blood pressure: manual check.
- Basic telemetry rhythm interpretation Q 4 hours and with significant rhythm changes
- Anti-embolism device integrity, placement and settings.

The RN is responsible for interventions:

- Position to optimize cardio-pulmonary function and minimize worsening of dependent edema.
- Remove anti-embolism devices for skin/tissue/edema assessment/care Q shift and for bathing, ambulation, prn. Reapply as soon as possible (ASAP).

####4. Respiratory

The RN is responsible for assessment and surveillance of:

- Breath sounds (anterior and posterior, all quadrants):  Auscultate.
- Adequacy of airway.
- Respiratory rhythm/pattern, depth, symmetry and use of accessory muscles.
- Respiratory rate (RR):  Visual inspection and count.
- Pulse oximetry.
- Oxygen delivery system (e.g. nasal cannula, face mask, non-rebreather mask, humidifier) and Liters/min, FIO2.
- Cough for productivity (character, color, amount).
- Chest tubes:
    - Location
    - Integrity of system connections, amount of suction, presence of air leak
    - Dressing condition/security and change date
    - Amount/appearance of secretions

The RN is responsible for interventions:

1.  Oral care.
2.  Position head of bed (HOB) ≥ 30° to maintain patent airway and to optimize ventilation and oxygenation.
3.  Encourage cough and deep breathing/incentive spirometer and mobilization out of bed.
4.  Initiate aspiration precautions.
5.  Suction set up.
6.  Oral suctioning.
7.  Maintain and monitor chest tubes (output, patency, and securement).
    5.     Gastrointestinal and Nutrition
The RN is responsible for assessment and surveillance of:
1.  Ability to swallow.
2.  Ability to tolerate oral (PO) intake and nutrition.  Appetite and presence of nausea/vomiting.
3.  Abdominal assessment: appearance (flat, distended), bowel sounds (active, diminished, absent, pitch), palpation (soft, firm), flatus and bowel movement (BM) (last BM, color, consistency, amount, frequency).
4.  Measured weight on admission and as indicated.
5.  Nasogastric (NG)/ Orogastric (OG) tube: ,Verify position, securement, skin/mucosal site condition, suction, output amount, color and character. Reference policy CP.155, Nutrition: Enteral
6.  Enteral feedings: tube securement, insertion site (mucosa/skin) condition, tube position, enteral nutrition type and rate, residual volumes.  Reference policy CP.155, Nutrition: Enteral
7.  Stoma: site, type, color, output amount and character/consistency, integrity of bag/drainage system.
The RN is responsible for interventions:
1.  Provide skin care.
2.  Position HOB ≥ 30°.
3.  Ensure dentures in place.
4.  Registered dietician referral.
5.  Verify placement of newly inserted NG/OG tubes by x-ray and confirmed by Radiologist or physician before use.
6.  Document tolerance, volumes and residuals for enteral feedings.
7.  Maintain patency of drainage and feeding tubes/security of drainage tubes.
8.  Perform stoma care: bag change
9.  Manage fecal management systems.
    6. Genitourinary & Intake and Output (I/O)    
The RN is responsible for assessment and surveillance of:
1.  All sources/volumes of intake and output.
2.  Ability to void including urinary drainage devices (urostomy, suprapubic cath, etc) and character of urine.
3.  Catheter or stoma site condition.
The RN is responsible for interventions:
Measure all intake and output.
Daily weights, if accurate I/O not possible.
Catheter Acquired Urinary Tract Infections (CAUTI) Prevention Strategies present/completed:
a.   Need for indwelling urinary catheter (IUC) screened Q shift and prn; discuss plan to remove catheter with provider if
        indications for use are not present or clear;
b.   Discontinue use of IUC once patient criteria met. Reference SP.115, Indwelling Urethral Catheter, Adult, Discontinuance of  
c.   Drainage bag below level of bladder and tubing unobstructed with kinks/loops.
d.   Catheter /tubing secured to reduce tension.
e.   Aseptic technique when accessing system if needed.
f.    Drainage bag emptied routinely ensuring drainage spout does not touch receptacle and to prevent overfilling.  
Catheter securement and perineal care.
    7.     Integumentary
The RN is responsible for assessment and surveillance of:
Condition of oral and nasal mucosa.
Skin assessment: General appearance and integrity of skin:  Condition of skin anterior and posterior, at pressure points, and skin folds. Braden score Q shift.
Condition of skin under devices and barriers (oxygen tubing, indwelling urinary catheters, SCDs, casts, splints, immobilizers, BiPAP/CPAP mask, etc.)
Initiate and/or assess type of pressure relief devices in use and settings (e.g. air mattress, specialty bed) and adequacy of pressure reduction by evaluating patient load skin condition, and patient comfort. Reference policy CP.43, Support Surface & Specialty Bed Selection.
Wounds: location, appearance, measurement, drainage and plan of care (treatments, wound vac, wound nurse consultation).
The RN is responsible for interventions:
Bath and linen change daily.
Position to minimize complications of pressure, friction and moisture.
Pressure ulcer prevention and wound care: Pressure Ulcer orders and/or plan of care.
Secondary Skin Assessments.
    8.      Musculoskeletal & Functional Mobility
The RN is responsible for assessment and surveillance of:
Functional status, strength, ability to ambulate, and level of assistance needed.
Range of motion (ROM) of all extremities.
Environment for safety hazards.
The RN is responsible for interventions:
Mobilize out of room if not contraindicated. Use assistive devices as needed. Bedrest orders require indication.
Assist with active/passive ROM if immobile.
Physical/Occupational Therapy consultation.
II     NEEDS ASSESSMENT
      Includes family and significant others as indicated/appropriate.
1.    Caring Environment/Behaviors
The RN is responsible for assessment and surveillance of:
Creates and maintains a healing environment.
Listen to concerns and clarify questions. (Duffy, 2009; Smith, Turkel & Wolf, 2013).
Identify what is important to the patient.
The RN is responsible for interventions:
Utilize authentic caring presence and develops a helping, trusting relationship (Watson, 2008). Call patients by name (Duffy, 2009).
Utilize mutual problem solving with patients, families and significant others that help them to understand how to confront, learn and  think about their health and illness.(Duffy, 2009).
Interaction will be respectful and supportive of the patient and/ family needs.
Communicate at the bedside and include the patient in the discussion. Provide information for decision-making (Smith, Turkel & Wolf, 2013).
Provide attentive reassurance (Duffy, 2009) which refers to being available and having a hopeful outlook. Encouraging manner.
Remove noxious stimuli: Noise, light and interruption control. Maintenance of privacy, safety and control.
Promote uninterrupted periods of rest.
Support  spiritual, emotional and belonging needs: referrals as needed (Chaplain, Patient, Family Services),
Support and maintain basic, human needs (Duffy, 2009). Decrease pain and suffering (Smith, Turkel & Wolf, 2013).
Explain procedures, treatments, and medications (Smith, Turkel & Wolf, 2013).
    2.     Comfort Management  (Reference policy CP.159 – Pain Management, Standards for Adult)
The RN is responsible for assessment and surveillance of:
Comfort level:
• Pain location, intensity (0-10 score if patient can self report or use nonverbal scale), characteristics, nonverbal signs symptoms  (HR, BP, diaphoresis, grimacing, guarding, etc) duration, onset. *If unable to assess pain, record why unable to assess (coma, heavily sedated, agitated, sleeping after intervention, etc).
• Anxiety/agitation level
• Sedation level
Response to/effectiveness of interventions.
Interventions which have previously helped manage pain, anxiety, and agitation.
The RN is responsible for interventions:
Provide non-phamacologic comfort measures (e.g. emotional support, positioning, guided imagery, massage, etc.).
Administer analgesics, anti-anxiety and sedation as ordered for pain and/or as prophylaxis before routine care or interventions which may cause pain/discomfort.
    3.    Psychosocial, Cultural, Spiritual & Communication  
The RN is responsible for assessment and surveillance of:
Favored methods of communication.
Use of coping strategies and demeanor.
Spiritual needs.
Needs related to current hospitalization or post hospital care and management.
Need for participation in pass code program, patient privacy and confidentiality. Reference policy AP.49, No Information/No Presence in Facility Patient Status
End of life needs/wishes.
The RN is responsible for interventions:
Provide information (NARRATE THE CARE) and reassurance (utilizing RELATE methodology and WORDS THAT WORK).
Keep patient informed of ordered tests and studies.
Provide tools to facilitate communication (white boards, interpreters, pen and paper, hearing aids, glasses,  etc.).
Referral to Patient Family Services, Chaplaincy or other appropriate services.
    4.   Plan of Care
The RN is responsible for assessment and surveillance of:
An individualized, goal-directed care plan will be created utilizing the nursing process from admission through discharge.
Patient data and identify and validate an appropriate patient-centered problem.
A patient-centered plan of care with measurable outcomes and nursing interventions.
The RN is responsible for interventions:
Collects patient data utilizing physical assessment and interviewing techniques (From Nursing Diagnosis Handbook, Ackley & Ladwig, 2008).
Initiate the plan of care and performs interventions.
Collaborate with multidisciplinary team on patient assessment, problem identification, plan of care, intervention/management strategies and evaluation of care.
Evaluate outcome(s) and appropriateness of the interventions in meeting the needs of the patient.
Review and revision of the plan of care.
Use of chain of command to notify successive providers and obtain timely resolution of an unresolved patient care problem. Reference policy NS.05, Chain of Command for Resolving Clinical Issues with Medical Staff.
    5.   Teaching & Discharge Planning
The RN is responsible for assessment and surveillance of:
Learning readiness and preference. Preferred/primary language.
Discharge planning needs, potential disposition upon discharge on admission.
Socio-economic factors, cultural and spiritual needs.
The RN is responsible for interventions:
Communicate assessment, interventions and plan of care to receiving RN before transfer to other floor or facility.
Follow up on identified needs with the appropriate resource/referrals (Patient Family Services, Home Health, Advanced Practice Nurse, Clinical Nurse Specialist (CNS), medical provider, Registered Dietician, chaplain, etc.).
Complete pre-operative and pre-procedural teaching (EGD, bronchoscopy, central line insertion, tests/procedures, etc).
Initiate and/or participate in patient care conference or family conference.
Complete discharge teaching prior to patient discharge. Validate understanding.
III     SAFETY ASSESSMENT
1. Environmental Check
The RN is responsible for assessment and surveillance of:
Environment free of clutter (i.e. trash, unnecessary supplies) and equipment.
Floor is clean, dry and clear of obstructions.
All medical devices in use are safe and plugged in with cords secured to prevent entanglement.
The RN is responsible for interventions:
Assure the patient environment is clean and orderly.
Contact appropriate support staff (Environmental Services, Maintenance, Clinical Engineering) for environmental needs and/or equipment repair.
Educate others in the maintenance of a safe patient environment.
    2.  Infection Prevention and Monitoring
The RN is responsible for assessment and surveillance of:
Temperature Monitoring source and method is chosen in order of most accurate and reliable:
1) By mouth (PO): unless contraindicated (i.e. face mask O2, drinking fluids, dry mouth, etc.)
2) Rectal: unless contraindicated (i.e. neurtropenia, thrombocytopenia, etc.)
3) Temporal.
4) Axillary: last option since least accurate/reliable.
Presence of precautions (contact, airborne, droplet), use of Personal Protective Equipment (PPE).
Skin: insertion/incision sites and output/secretions for signs of infection.
Condition of peripheral and central line insertion sites.  
Drain/tube patency, location, type and site.
Drainage: appearance and volume.
The RN is responsible for interventions:
Perform hand hygiene before and after patient/patient environment contact.
Use standard precautions and personal protective equipment (PPE).
Ensure precautions are maintained.
Maintain tubes and lines per nursing procedures.
Change peripheral IV sites Q 72 hours or obtain a provider order to maintain site > 72 hours.   Change peripheral site prn signs/symptoms of infiltration, phlebitis, etc.  Reference policy CP.15, IV Therapy.
Catheter site will be changed if suspected contamination or within 24 hours if inserted during emergent situations. Reference policy CP.15, IV Therapy.
Maintains aseptic technique with medication administration, during invasive procedures, dressing changes, and when working with IV/central venous lines and tubing.
CLABSI Prevention Strategies are present/completed:
1) Ensure provider uses maximum sterile barrier precautions with central line insertion when present.
2) Need for central line screened daily (monitoring, medications, therapies, rapid volume infusion, or unable to obtain alternate IV access)-discuss removal of central line if indications not present with provider,
3) Maintain a closed system and use “scrub the hub” techniques prior to accessing hubs/lumens.
4) Central line site and dressing assessment as care per nursing procedure.
IV Dressing changes per policy CP.15, IV Therapy & CP.06, IV therapy: Care of Central Venous Access Devices.
1) Peripheral IV transparent dressings Q 72 hours and prn for compromised dressing integrity.  
2) Central line dressing are changed Q 7 days if transparent dressing.
3) Gauze dressing are changed Q 48 hours even when covered by transparent dressings.
    3.     Risk Assessment and Screening
The RN is responsible for assessment and surveillance of:
Pressure ulcer risk. Reference policy DC.02, Documentation Acute Patient Care.
Fall risk. Reference policy DC.02, Documentation Acute Patient Care.
Venous Thromboembolism (VTE). Reference policy DC.02, Documentation Acute Patient Care.
Nutritional Risk. Reference policy DC.02, Documentation Acute Patient Care.
Functional. Reference policy DC.02, Documentation Acute Patient Care.
Abuse and neglect. Reference policy AP.66, Suspected Child and /or Elder/Dependent Adult Abuse Reporting.
Restraint use. Reference policy CP.24, Restraint/Seclusion of Patients.
Suicide risk. Reference policy  CP.26, Suicide Potential: Care of Patients with Threatened or Actual Suicide Attempted-Acute Care.
CAGE substance abuse screening tool.
The RN is responsible for interventions:
Bed rails up times 2, bed in low position, wheels locked, brakes locked.
Call light and phone accessible to patient.
Perform hourly rounding Q 1 hour 0600-2200 and Q 2 hours 2200-0600.
Monitor that equipment and therapeutic equipment are intact and attached appropriately (e.g. Tabs units, bed alarms, IV pumps, pulse oximetry).
Apply, release, and discontinue restraints. Reference policy CP.24, Restraint/Seclusion of Patients.
    4.     Medication Management      
The RN is responsible for assessment and surveillance of:
Upon admission and throughout the patient’s stay, review and create home medication list. Include indication for use: if unclear, clarify with provider. Important to validate how meds are actually taken as this may vary from directions listed on containers.  Enter home pharmacy.
IV fluids & blood product infusions.
IV insertion sites and line types (peripheral, subcutaneous, central).
Medication verification for high alert medications. Reference policy CP.19, Medication: Administration.
Accurate IV pump programming and use of Guardrails safety feature.
The RN is responsible for interventions:
Complete medication order review and acknowledgment prior to administration (exception is urgent or emergent situations but should be completed as soon as patient condition/situation is safe to do so).
Administer medication per medical center and nursing administration policies and procedures including the use of Bar Code Medication Administration (BCMA) and the “6 Rights,”: Right drug, right dose/units, right route, right time/frequency, right patient. WHERE IS THIS 6 RIGHTS COMING FROM – RECOMMEND CHANGING BACK TO 5 RIGHTS
Document medication administration including
1) drug
2) dose (concentration)
3) units of delivery (i.e. mcg, units, mg, gm, etc.)
4) route
5) site of injection if IM or SQ.
Waste with witness any unused controlled substances.
Administer blood and blood products. Reference policy CP.28, IV Therapy: Transfusion of Blood and Blood Products. Observe for adverse reactions.
    5.     Tests/Procedures
The RN is responsible for assessment and surveillance of:
Completion of result (i.e. radiology, hematology, blood gases, chemistry, microbiology, etc.).
The RN is responsible for interventions:
Appropriate identification, labeling and requisition completion during nurse collected specimen collection.
Notify provider with significant changes, abnormal values, stats, and critical values. Reference policy CP.146, Critical Results/Values, Provider Notification of.
6.     Order Management
The RN is responsible for assessment and surveillance of:
Orders should be processed, acknowledged, noted, and/or implemented accurately within 2 hours of being ordered for routine and urgent orders and within 15 minutes within being ordered “STAT”. Reference policy DC.06, Orders: Processing and Notation of Non-Medication Orders.
Orders pending activation will be activated within 60 minutes of patient’s arrival to the department.
The medical record should be reviewed Q 2 hours for the presence of orders to be acknowledged and at the end of shift.
Orders must be reviewed by the RN prior to implementation.
Telephone and verbal orders will be written down, read back and confirmed with the individual who gave the order.
The RN is responsible for interventions:
Orders will be reconciled and acknowledged in the electronic medical record prior to notation of orders.
Appropriate transcription within the medical record.
Incomplete, unclear or conflicting orders will be clarified.

