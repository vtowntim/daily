exports.like = (req, res) ->
	social = require("../models/social")
	social.like req.param("spId"), req.param("type"), req.param("user"), req.param("name"), (err) ->
		if err
			console.error err
		else
			social.getSocial req.param("spId"), req.param("type"), (err, socialData) ->
				res.jsonp socialData
				return

		return

	return



exports.getContentByUser = (req, res) ->
	content = require('../models/content')
	username = req.param('user')
	page = req.param('page') or 1
	pageSize = req.param('pageSize') or 10
	content.getContentByUsername username, page, pageSize, (err, contentByUser) ->
		console.error(err) if err
		res.send contentByUser
		return
	return


exports.saveContent = (req, res) ->
	content = require('../models/content')
	return



exports.comment = (req, res) ->
	social = require("../models/social")
	social.comment req.param("spId"), req.param("type"), req.param("user"), req.param("name"), req.param("comment"), (err) ->
		if err
			console.error err
		else
			social.getSocial req.param("spId"), req.param("type"), (err, socialData) ->
				res.jsonp socialData
				return

		return

	return






exports.trackMail = (req, res) ->
	listID = req.param("listID")
	user = req.param("user")
	type = req.param("type") or "DDaily"
	hits = require("../models/hits")
	hits.makeHit listID, type, user, true
	res.sendfile "public/images/pixel.png"
	return






exports.getList = (req, res) ->
	districtDaily = require("../models/districtDaily")
	moment = require("moment")
	listDate = moment(req.param("date")).hour(12).minute(0).format("YYYY-MM-DD HH:mm")
	console.log req.param("date")
	console.log listDate
	districtDaily.getList listDate, (err, list) ->
		res.jsonp list
		return

	return






exports.getSocial = (req, res) ->
	social = require("../models/social")
	social.getSocial req.param("spId"), req.param("type"), (err, socialData) ->
		if err
			console.error err
		else
			res.jsonp socialData
		return

	return






exports.getContent = (req, res) ->
	quotes = require("../models/scheduledContent")
	quotes.getContent req.param("date"), (err, quotes) ->
		if err
			console.error err
		else
			res.jsonp quotes
		return

	return






exports.saveContentOLD = (req, res) ->
	quotes = require("../models/scheduledContent")
	contentObj =
		date: req.param("date")
		type: req.param("type")
		subject: req.param("subject")
		content: req.param("content")
		meta: req.param("meta")

	quotes.saveContent contentObj, (err, quotes) ->
		if err
			console.error err
		else
			res.jsonp status: "Saved!"
		return

	return






exports.searchQuotes = (req, res) ->
	quotes = require("../models/scheduledContent")
	quotes.searchQuotes req.param("term"), (err, quotes) ->
		if err
			console.error err
		else
			res.jsonp quotes
		return

	return






exports.usernames = (req, res) ->
	kdPeople = require("../models/kdPeople")
	kdPeople.getEmployeeUsernames (error, usernames) ->
		res.send JSON.stringify(usernames)  if usernames
		return

	return