
exports.ping = (myreq, myres) ->
	utils = require("../models/utils.coffee")
	moment = require("moment")
	theIP = utils.getClientAddress(myreq)
	console.log "Ping from SSIS! - " + moment().format() + " - " + theIP
	myres.send "Ping from DDaily!"
	return






exports.list = (myreq, myres) ->
	moment = require("moment")
	today = (if moment().hour() >= 12 then moment() else moment().subtract("d", 1))
	console.log moment().hour()
	myres.redirect "/" + today.format("YYYY/MM/DD") + "/"
	return






exports.updateEvents = (req, res) ->
	events = require("../models/events")
	events.getEventsFromSharePoint (err) ->
		if err
			console.error err
		else
			res.send "Updated Events"
		return

	return






generateTheDaily = (callback) ->
	districtDaily = require("../models/districtDaily")
	console.log "Manually generating the District Daily"
	districtDaily.build (err, output) ->
		if err
			console.error err
		else
			districtDaily.gatherContent new Date(), callback
		return

	return






exports.genList = (myreq, myres) ->
	generateTheDaily (err, content) ->
		console.error err  if err
		myres.json content
		return

	return






exports.generate = (req, res) ->
	#res.send(req.param('return'));
	if req.param("return")
		generateTheDaily (err, content) ->
			console.error err  if err
			res.redirect req.param("return")
			return

	else
		res.send 404, "Sorry, we cannot find that!"
	return






exports.announcement = (myreq, myres) ->
	districtDaily = require("../models/districtDaily")
	thisIsATest = true
	districtDaily.announcement myres, thisIsATest, (err, content) ->
		console.error err  if err
		return

	return






exports.announcementView = (myreq, myres) ->
	districtDaily = require("../models/districtDaily")
	md = require("marked")
	content =
		title: "Announcing the District Daily"
		md: md

	myres.render "announcement", content
	return





exports.emailList = (myreq, myres) ->
	districtDaily = require("../models/districtDaily")
	districtDaily.prepareAndSendTheDaily myres, true, (err, content) ->
		console.error err  if err
		return

	return

	

exports.resend = (myreq, myres) ->
	districtDaily = require("../models/districtDaily")
	thisIsATest = false

	districtDaily.prepareAndSendTheDaily myres, thisIsATest, (err, content) ->
		console.error err  if err
		return

	return



exports.resendTest = (myreq, myres) ->
	districtDaily = require("../models/districtDaily")
	thisIsATest = true

	districtDaily.prepareAndSendTheDaily myres, thisIsATest, (err, content) ->
		console.error err  if err
		return

	return




exports.eventList = (myreq, myres) ->
	events = require("../models/events")
	events.getEventsList (err, records) ->
		console.error err  if err
		myres.json records
		return

	return






exports.makeEventList = (myreq, myres) ->
	events = require("../models/events")
	events.makeEventsList (err, records) ->
		console.error err  if err
		myres.json records
		return

	return


exports.getFeaturedEvents = (req, res)->
	events = require("../models/events")
	events.getFeaturedEvents (err, recordset)->
		console.error err if err
		res.send recordset
	return




renderContent = (myreq, myres, jade, content, json) ->
	content.pageType = "list"
	if json
		myres.send content
	else
		myres.render jade, content, (err, html) ->
			console.error err  if err
			myres.send html.replace(/\??u=\?/g, "")
			return

	return







exports.submitForm = (req, res) ->
	utils = require("../models/utils.coffee")
	utils.checkForLogin req, res, (userData) ->
		res.render "site/submitForm",
			title: "Submit to the District Daily"
			employee: req.params.employee
			userData: userData

		return
	return





exports.getList = (myreq, myres) ->
	districtDaily = require("../models/districtDaily")
	moment = require("moment")

	jade = myreq.param("jade") or "site/districtDaily"
	json = (if myreq.param("json") then true else false)

	if myreq.param("testData")

		testData = require("../models/testData").day

		testData = districtDaily.modifyContent testData

		renderContent myreq, myres, jade, testData, json

	else
		listDate = moment(myreq.params[0]).hour(10).format("YYYY-MM-DD HH:mm")
		console.log "getting list for "
		console.log listDate
		districtDaily.getList listDate, (err, content) ->
			unless content

				districtDaily.gatherContent listDate, (err, content) ->
					content = districtDaily.modifyContent content
					renderContent myreq, myres, jade, content, json
					return

			else
				content = districtDaily.modifyContent content
				renderContent myreq, myres, jade, content, json
			return

	return






exports.getUser = (myreq, myres) ->
	utils = require("../models/utils.coffee")
	kdPeople = require("../models/kdPeople")
	kdPeople.getEmployeeUsernames (err, usernames) ->
		console.error err  if err
		if usernames
			console.log "We have usernames!"
			# blastTheDailyEmail(usernames, emailBodyHTML, httpResponse, subject);
			myres.send usernames
		return

	return







renderRecord = (pageData, myreq, myres) ->
	switch pageData.verb
		when "post"
			districtDaily = require("../models/districtDaily")
			us = require("underscore")._
			districtDaily.getPost pageData.post, (err, results) ->
				console.error err  if err
				theAttachments = []
				results.attachments.forEach (attachment) ->
					theAttachments.push attachment  unless results.recordset[0].body.indexOf(attachment.path) + 1
					return

				recordData =
					dDaily: dDaily
					attachments: theAttachments
					list: pageData.list
					pageType: "record"
					record: results.recordset[0]
					title: results.recordset[0].title
					user: pageData.theUser
					userData: pageData.userData
					post: pageData.post

				districtDaily.getList recordData.list.replace(/\//g, "-") + " 12:00", (err, listData) ->
					# recordData.listData = listData;
					recordData.listData = (if listData then us.union(listData.records.vip, listData.records.news) else [])
					myres.render "site/record", recordData
					return

				hits = require("../models/hits")
				hits.makeHit pageData.post, "Content", pageData.theUser or pageData.theIP
				return

		when "focus"
			districtDaily = require("../models/districtDaily")
			us = require("underscore")._
			districtDaily.getFocus pageData.post, (err, results) ->
				console.error err  if err
				recordData =
					dDaily: dDaily
					list: pageData.list
					pageType: "focus"
					record: results.recordset[0]
					title: results.recordset[0].subject
					user: pageData.theUser
					userData: pageData.userData
					post: pageData.post

				districtDaily.getList recordData.list.replace(/\//g, "-") + " 12:00", (err, listData) ->
					# recordData.listData = listData;
					recordData.listData = us.union(listData.records.vip, listData.records.news)
					myres.render "site/record", recordData
					return

				hits = require("../models/hits")
				hits.makeHit pageData.post, "Focus", pageData.theUser or pageData.theIP, true
				return

		when "event"
			districtDaily = require("../models/districtDaily")
			us = require("underscore")._
			districtDaily.getEvent pageData.post, pageData.occurrence, (err, results) ->
				console.error err  if err
				recordData =
					dDaily: dDaily
					list: pageData.list
					pageType: "event"
					record: results.recordset
					title: results.recordset.title
					user: pageData.theUser
					userData: pageData.userData
					post: pageData.post
					occurrence: pageData.occurrence
					path: require("url").parse(myreq.url).pathname

				recordData.listData = []
				unless pageData.otherFunction is "ical"
					myres.render "site/record", recordData
					hits = require("../models/hits")
					hits.makeHit pageData.post, "Events", pageData.theUser or pageData.theIP
				else
					iCalEvent = require("icalevent")
					moment = require("moment")
					myEvent = new iCalEvent()
					myRecord = recordData.record
					if myRecord.recurrence
						$occurrenceDate = moment(recordData.occurrence.match(/(\w*)\./)[1] + "-01-01").add(parseInt(recordData.occurrence.match(/\.(\w*)/)[1]), "d")
						startDate = $occurrenceDate.hour(moment(myRecord.startDate).hour()).format()
						endDate = $occurrenceDate.hour(moment(myRecord.endDate).hour()).format()
					else
						startDate = moment(myRecord.startDate).format()
						endDate = moment(myRecord.endDate).format()
					myEvent.set "offset", new Date().getTimezoneOffset()
					myEvent.set "method", "request"
					myEvent.set "start", startDate
					myEvent.set "end", endDate
					myEvent.set "summary", myRecord.title
					myEvent.set "description", myRecord.body
					myEvent.set "location", myRecord.location
					# myEvent.set('organizer', { name: 'Nacho Libre', email: 'luchador@monastery.org' });
					myEvent.set "url", "http://daily.kdhcd.org/" + recordData.list + "/E" + recordData.post + "-" + recordData.occurrence
					myres.type "text/calendar"
					myres.send myEvent.toFile()
				return


		# myres.send(recordData)
		when "attachments"
			content = require("../models/content")
			content.getAttachments pageData.post, (err, recordset) ->
				us = require("underscore")._
				myres.json paths: us.pluck(recordset, "path")
				return

		when "data"
			hits = require("../models/hits")
			moment = require("moment")
			postType = (if pageData.isEvent then "Events" else "Content")
			hits.countDetail pageData.post, postType, (err, data) ->
				if err
					console.error err
				else
					data.forEach (hit, index) ->
						hit.date = moment(hit.date).format("YYYY-MM-DD HH:mm:ss")
						return

					us = require("underscore")._
					myres.render "site/data",
						title: "Hit Data for " + pageData.post
						post: pageData.post
						columns: us.keys(data[0])
						rows: data

				return

		when "dataDownload"
			hits = require("../models/hits")
			moment = require("moment")
			csv = require("csv.js")
			postType = (if pageData.isEvent then "Events" else "Content")
			hits.countDetail pageData.post, postType, (err, data) ->
				if err
					console.error err
				else
					data.forEach (hit, index) ->
						hit.date = moment(hit.date).format("YYYY-MM-DD HH:mm:ss")
						return

					myres.set "Content-Type", "application/csv"
					myres.set "Content-disposition", "attachment;filename=DistrictDaily_" + post + "_data.csv"
					myres.send csv(data)
				return







exports.getEvent = (myreq, myres) ->
	getRecord myreq, myres, "event"
	return






exports.getFocus = (myreq, myres) ->
	#myres.send('Building...');
	getRecord myreq, myres, "focus"
	return





getRecord = (myreq, myres, verb) ->
	utils = require("../models/utils.coffee")
	kdPeople = require("../models/kdPeople")
	pageData = {}
	if verb is "event"
		pageData.isEvent = true
		pageData.verb = verb
		pageData.occurrence = myreq.params[2]
		pageData.otherFunction = myreq.params[3]
	else if verb is "focus"
		pageData.isFocus = true
		pageData.verb = verb
		pageData.occurrence = myreq.params[2]
	else
		pageData.verb = myreq.params[2] or "post"
	pageData.post = myreq.params[1]
	pageData.list = myreq.params[0]
	pageData.theIP = utils.getClientAddress(myreq)
	if myreq.query.u
		user = utils.decrypt(myreq.query.u)
		myres.cookie "user", user
		myres.clearCookie "userData"
		postURLSegment = pageData.post
		if pageData.isEvent
			postURLSegment = "E" + postURLSegment
			postURLSegment = postURLSegment + "-" + myreq.params[2]  if myreq.params[2]
		postURLSegment = "F" + postURLSegment  if pageData.isFocus
		#postURLSegment = pageData.isEvent ? 'E' + pageData.post : pageData.post; // If this is an event, add an 'E' before the post id.
		myres.redirect "/" + pageData.list + "/" + postURLSegment + "/"
		return false
	pageData.theUser = myreq.cookies.user
	if myreq.cookies.user and not myreq.cookies.userData
		kdPeople.getUser pageData.theUser, (err, userData) ->
			myres.cookie "userData", utils.encrypt(userData)
			pageData.userData = userData
			renderRecord pageData, myreq, myres
			return

	else
		pageData.userData = (if myreq.cookies.userData then utils.decrypt(myreq.cookies.userData) else null)
		renderRecord pageData, myreq, myres
	return


exports.getRecord = getRecord





exports.sharepointImport = (myreq, myres) ->
	districtDaily = require("../models/districtDaily")
	districtDaily.build (err, output) ->
		if err
			console.error err
		else
			myres.send output
		return

	return