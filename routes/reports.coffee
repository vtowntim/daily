reports = {}



reports.count = (req, res, param) ->
	reports = require("../models/reports")
	reports.count (err, data) ->
		res.send data[0][0]
		return

	return



reports.hits = (req, res, rundate) ->
	reports = require("../models/reports")
	reports.mailHits req, res, rundate, false
	return


reports.hitsdata = (req, res, rundate) ->
	reports = require("../models/reports")
	reports.hitsCSV req, res, rundate
	return


reports.mailHits = (req, res, rundate) ->
	reports = require("../models/reports")
	reports.mailHits req, res, rundate, true
	return
exports.mailHits = reports.mailHits


reports.recognitions = (req, res, days) ->
	reports = require("../models/reports")
	reports.recognitions req, res, days
	return


reports.transcribedRecognitions = (req, res, days) ->
	reports = require("../models/reports")
	reports.transcribedRecognitions req, res, days
	return

reports.EOM = (req, res, days) ->
	reports = require("../models/reports")
	reports.EOM req, res, days
	return

reports.transcribedEOM = (req, res, days) ->
	reports = require("../models/reports")
	reports.transcribedEOM req, res, days
	return


exports.makeReport = (req, res) ->
	reportName = req.params.reportName
	param = req.params.param1
	unless reportName
		res.send 404
		return false
	reports[reportName] req, res, param
	return