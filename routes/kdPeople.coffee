exports.usernames = (req, res) ->
	kdPeople = require("../models/kdPeople")
	kdPeople.getEmployeeUsernames (error, usernames) ->
		res.send JSON.stringify(usernames)  if usernames
		return

	return







exports.searchEmployees = (req, res) ->
	kdPeople = require("../models/kdPeople")
	if req.query.term
		kdPeople.searchEmployees req.query.term, (error, employees) ->
			console.error error  if error
			res.jsonp employees[0]
			return

	else
		res.send 200
	return



exports.searchPhysicians = (req, res) ->
	kdPeople = require("../models/kdPeople")
	if req.query.term
		kdPeople.searchPhysicians req.query.term, (error, employees) ->
			console.error error  if error
			res.jsonp employees[0]
			return

	else
		res.send 200
	return





exports.getRecognition = (req, res) ->
	kaweahCare = require("../models/kaweahCare")
	kaweahCare.getRecognition (err, recognizedPeopleUsernames) ->
		res.send recognizedPeopleUsernames
		return

	return





exports.insertRecognition = (req, res) ->
	kaweahCare = require("../models/kaweahCare")
	moment = require("moment")
	transcribed = req.param("transcribed") or null
	recogintionObj =
		id: (if req.param("id") then req.param("id") else 9999999)
		recognized: (if req.param("recognized") then req.param("recognized") else moment().format("YYYY-MM-DD HH:mm:ss"))
		employee: req.param("employee")
		supervisor: req.param("supervisor")
		sender: (if transcribed is "true" then null else req.param("sender"))
		description: req.param("description")
		recognizee: req.param("recognizee")
		type: req.param("type")

		transcribed: transcribed is "true"
		transcribedBy: (if transcribed is "true" then req.param("sender") else null)

	res.send 200
	kaweahCare.insertRecognition recogintionObj
	return




exports.insertEOM = (req, res) ->
	kaweahCare = require("../models/kaweahCare")
	moment = require("moment")
	transcribed = req.param("transcribed") or null
	recogintionObj =
		id: (if req.param("id") then req.param("id") else 9999999)
		recognized: (if req.param("recognized") then req.param("recognized") else moment().format("YYYY-MM-DD HH:mm:ss"))
		employee: req.param("employee")
		supervisor: req.param("supervisor")
		sender: (if transcribed is "true" then null else req.param("sender"))
		description: req.param("description")
		recognizee: req.param("recognizee")
		transcribed: transcribed is "true"
		transcribedBy: (if transcribed is "true" then req.param("sender") else null)

	res.send 200
	kaweahCare.insertEOM recogintionObj
	return





exports.recogintion = (req, res) ->
	utils = require("../models/utils.coffee")
	utils.checkForLogin req, res, (userData) ->

		jade = req.param("jade") or "site/KaweahCare-Recognition"

		res.render jade,
			title: "Kaweah Care Recognition"
			employee: req.params.employee
			userData: userData

		return

	return


exports.recogintion2 = (req, res) ->
	utils = require("../models/utils.coffee")
	utils.checkForLogin req, res, (userData) ->

		jade = req.param("jade") or "site/KaweahCare-Recognition-New"

		res.render jade,
			title: "Kaweah Care Recognition (2)"
			employee: req.params.employee
			userData: userData

		return

	return
