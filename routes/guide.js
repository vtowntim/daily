var getMDFile = function(filename, callback) {
    var fs = require("fs");
    fs.readFile('./md/' + filename + '.md', callback);
}


exports.style = function(req, res) {

    var md = require('marked');

    getMDFile('StyleGuide', function(error, mdFile) {
        res.render('site/guide', {
            title: 'District Daily Style Guide',
            md: md,
            theContent: mdFile
        });
    });

    var hits = require('../models/hits');
    hits.quickHit('StyleGuide', req);

};

exports.policy = function(req, res) {

    var md = require('marked');

    getMDFile('Policy', function(error, mdFile) {
        res.render('site/guide', {
            title: 'District Daily Policies',
            md: md,
            theContent: mdFile
        });
    });

    var hits = require('../models/hits');
    hits.quickHit('Policy', req);

};


exports.soc = function(req, res) {

    var md = require('marked');

    getMDFile('soc', function(error, mdFile) {
        res.render('site/guide', {
            title: 'SOC',
            md: md,
            theContent: mdFile
        });
    });

};