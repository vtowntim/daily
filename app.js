 console.log(new Date().toString());
 /**
  * Module dependencies          .
  */


 var express = require('express');
 var path = require('path');
 require('coffee-script/register');

 var routes = require('./routes');
 var ping = require('./routes/ping');
 var api = require('./routes/api');


 var records = require('./routes/records');
 var guide = require('./routes/guide');
 var scheduledContent = require('./routes/scheduledContent');
 var reports = require('./routes/reports');
 var kdPeople = require('./routes/kdPeople');


 var http = require('http');


 //District Daily Settings
 dDaily = {
     dailyReviewers: ['tanderso', 'dvolosin', 'ddesimas', 'bcapwell', 'lflorez']
 };

 // dDaily.dailyReviewers = 'tanderso';

 var app = express();

 global.APPDIR = __dirname;

 app.locals.moment = require('moment');
 app.locals._ = require("underscore");

 // all environments
 app.set('port', process.env.PORT || 3000);
 app.set('views', path.join(__dirname, 'views'));
 app.set('view engine', 'jade');
 app.use(express.favicon());
 app.use(express.logger('dev'));
 app.use(express.json());

 app.use(express.urlencoded());
 app.use(express.methodOverride());
 app.use(express.cookieParser());
 app.use(app.router);
 // app.use(require('less-middleware')({
 //     src: path.join(__dirname, 'public')
 // }));
 app.use(express.static(path.join(__dirname, 'public')));

 app.enable('trust proxy');

 // development only
 //if ('development' == app.get('env')) {
 app.use(express.errorHandler());
 //}

 // app.get('/pingNoon', ping.NoonDeliver);

 app.get('/', records.list);
 app.get('/genList', records.genList);
 app.get('/generate', records.generate);
 app.get('/getUser', records.getUser);

 // app.get('/tim', function(req,res){ res.send('Coooool'); })

 app.get('/pingTim', ping.ping);
 app.get('/ping10', ping.TenReview);
 //app.get('/pingSendAgain', ping.NoonDeliver);


 app.get('/4U70M473D-5Y573M', ping.ping);
 app.get('/list', records.list);

 //app.get('/pingHit', ping.sendContentRankReport); // Use this to manually send out the district daily hits report

 app.get('/email', records.emailList);
 app.get('/resendDistrictDaily', records.resend);
 app.get('/resendPreview', records.resendTest);
 // app.get('/announcementMail', records.announcement);
 // app.get('/announcement', records.announcementView);


 app.get('/updateEvents', records.updateEvents);
 app.get('/events', records.eventList);
 app.get('/events2', records.makeEventList);
 app.get('/getFeaturedEvents', records.getFeaturedEvents)

 app.get('/submit', records.submitForm);

 app.get('/app', routes.index);
 app.get('/app/sharepointImport', records.sharepointImport);

 app.get('/style', guide.style);
 app.get('/policy', guide.policy);
 app.get('/soc', guide.soc);

 app.get('/test', function(req, res) {
     var kdPeople = require('./models/kdPeople');
     var us = require("underscore");
     kdPeople.getAnniversaries('2014-05-05', function(err, rows) {
         res.send(us.groupBy(rows[0], 'years'));
     })
 });

 app.get('/scheduledContent', scheduledContent.index);
 app.get('/api/searchQuotes', api.searchQuotes);
 // app.post('/api/scheduledContent', api.saveContent);
 app.get('/api/scheduledContent', api.getContent);

 app.get('/kdpeople/searchEmployees', kdPeople.searchEmployees);
app.get('/kdpeople/searchPhysicians', kdPeople.searchPhysicians);

 app.get('/recognition/:employee?', kdPeople.recogintion);

 app.get('/recognition2/:employee?', kdPeople.recogintion2);


 app.get('/recognitionEntry/:employee?', kdPeople.recogintion);
 app.post('/api/insertRecognition', kdPeople.insertRecognition);
 app.post('/api/insertEOM', kdPeople.insertEOM);

 app.get('/api/getRecognition', kdPeople.getRecognition);

 app.get('/trackMail', api.trackMail);

 app.get('/usernames', api.usernames);

 app.get('/report/:reportName/:param1?', reports.makeReport);

 app.post('/api/like', api.like);
 app.get('/api/list', api.getList);
 app.get('/api/userContent', api.getContentByUser);
 app.post('/api/userContent', api.saveContent);
 app.post('/api/comment', api.comment);
 app.get('/api/social', api.getSocial);

 app.get(/^\/(20\d\d\/[0-1]\d\/[0-3]\d)\/F([\d]+)-?([0-9]*\.[0-9]*)?\/?(attachments$|post$|data$|dataDownload$)?\/?/, records.getFocus);
 app.get(/^\/(20\d\d\/[0-1]\d\/[0-3]\d)\/([\d]+)\/?(attachments$|post$|data$|dataDownload$)?\/?/, records.getRecord);
 app.get(/^\/(20\d\d\/[0-1]\d\/[0-3]\d)\/E([\d]+)-?([0-9]*\.[0-9]*)?\/?(ical$|attachments$|post$|data$|dataDownload$)?\/?/, records.getEvent);
 app.get(/^\/(20\d\d\/[0-1]\d\/[0-3]\d)\/?/, records.getList);

 console.log(process.env.PORT);

 // app.get('/testData', routes.testData);
 // app.get('/users', user.list);

 http.createServer(app).listen(app.get('port'), function() {
     console.log('########## Express server listening on port ' + app.get('port'));
 });
