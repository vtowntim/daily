vm = {
	user: ko.observable(USER),
	spId: ko.observable(SPID),
	name: ko.observable(NAME || null),

	likes: ko.observableArray([]),
	likeNames: ko.observable(),
	comments: ko.observableArray([]),

	app: {
		bottomVisible: ko.observable(true)
	},

	liked: ko.observable(false),
	showLikeDetails: ko.observable(false)
}

ko.applyBindings(vm);

if(typeof console != "undefined"){console.clear();}

makeLikeNames = function(){
	var returnString;
	if (vm.liked()){
		var names = _.reject(vm.likes(), function(like){ return like.name == vm.name() });
		var likeNames = _.pluck( _.shuffle(names) ,'name').slice(0,3);
		returnString = 'You ';
	}else{
		var likeNames = _.pluck( _.shuffle(vm.likes()) ,'name').slice(0,3);
		returnString = '';
	}
	switch(vm.likes().length){
		case 0:
			return vm.user() ? 'Like this post!' : 'This post has 0 likes.';
			break;
		case 1:
			return vm.liked() ? 'You like this post.' : likeNames[0] + ' likes this post.';
			break;
		case 2:
			return vm.liked() ? 'You and ' + likeNames[0] + ' like this post.' : likeNames[0] + ' and ' + likeNames[1] + ' like this post.';
			break;
		case 3:
			return vm.liked() ? 'You, ' + likeNames[0] + ', and ' + likeNames[1] + ' like this post.' : likeNames[0] + ', ' + likeNames[1] + ', and ' + likeNames[2] + ' like this post.';
			break;
		default:
			return vm.liked() ? 'You, ' + likeNames[0] + ', ' + likeNames[1] + ', and ' + (vm.likes().length - 3) + ' others like this post.' : likeNames[0] + ', ' + likeNames[1] + ', ' + likeNames[2] + ', and ' + (vm.likes().length - 3) + ' others like this post.';
	}
}

processSocialData = function(socialData){

	vm.likes( _.sortBy(socialData.likes, 'name') );
	vm.comments(socialData.comments);
	vm.liked( _.findWhere(vm.likes(), {user: vm.user()}) ? true : false );
	vm.likeNames( makeLikeNames() );

}

viewLikes = function(){
	vm.showLikeDetails( vm.showLikeDetails() ? false : true );
}

prettyBigImages = function(){

	$('.body img').each(function(k,image){
		if ( image.width * image.height > 100000 ){
			$(image)
				.addClass('bigImage')
				.width('100%')
				.height('auto');
		}
	})

}


doLike = function(){
	var payload = {
			spId:	SPID || POST,
			type:	PAGETYPE,
			user:	USER,
			name:	NAME
		};
	$.post('/api/like', payload, processSocialData);
}

doComment = function(){
	var payload = {
			spId:	SPID || POST,
			type:	PAGETYPE,
			user:	USER,
			name:	NAME,
			comment: $('#newComment').val()
		};
	$.post('/api/comment', payload, processSocialData);
	$('#newComment').val('');
}


$(function(){

	prettyBigImages();

	var payload = {
			spId:	SPID || POST,
			type:	PAGETYPE
		};
	$.get('/api/social', payload, processSocialData)


	$('.commentLink button').click(function(){
		if( $('#comments').length ){
			location.href='#comments';
		}else{
			location.href='#social';
		}
	});

})


clickLike = function(){
	if(!USER) {
		location.href='#likes';
		return false;
	}
	$('.like').toggleClass('userLiked');
	doLike();
}

clickLikeThrottled = _.throttle( clickLike , 1500, {trailing: false} );


var floatNavs = false;
$("body").on("mousemove",function(event) {

    if (event.pageX < 100 || event.pageX > ($(window).width() - 100)) {
        if (!floatNavs){ $('.floatNav').fadeIn(); floatNavs = true;}
    }else{
    	if (floatNavs){ $('.floatNav').fadeOut(); floatNavs = false;}
    }

}).on('click', '.like button', function(){

	clickLikeThrottled();

});

// $('#docBody').waypoint(function(direction) {
// 	switch(direction){
// 		case 'down':
// 			vm.app.bottomVisible(true);
// 		break;
// 	}
// 	},
// 	{offset: function() {
// 		return - ( $(this).height() - $(window).height() ) / 2
// 		}
// 	}
// );

$('#social').waypoint(function(direction) {
	switch(direction){
		case 'up':
			if($(window).width() > 790){
				$('.socialSide').fadeIn();
			}
		break;
		case 'down':
			$('.socialSide').fadeOut();
		break;
	}
	},
	{offset: 'bottom-in-view'}
);



var leaving = function(element){
	location.href = $(element).attr('href');
	return false;
}