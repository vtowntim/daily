if(console){console.clear()}

vm = {
	newQuote: ko.observable(),
	newQuoteDate: ko.observable(),
	dates: ko.observableArray([]),
	scheduledContent: ko.observableArray([]),
	quoteSearch: ko.observable(),
	quotes: ko.observableArray([]),
	seltdItem: ko.observable()
}

item = function(newItem){
	newItem = newItem || {
		id: null, date: null, type: null, subject: null, content: null, meta: null
	};
	this.id = ko.observable(newItem.id);
	console.log(this.id);
	this.date = ko.observable(newItem.date);
	this.type = ko.observable(newItem.type);
	this.subject = ko.observable(newItem.subject);
	this.content = ko.observable(newItem.content);
	this.meta = ko.observable(newItem.meta);
};

ko.applyBindings(vm);

// findQuote = function(search){
// 	return _.filter(quotes, function(myString){
// 		return myString.indexOf(search) + 1;
// 		})
// 	}

$('#search').keyup(function(e){
	if (e.which == 13){
		searchQuote( $(this).val() );
	}
})


$('body').on('click','#quotePicker li',function(){
	vm.seltdItem().content( $(this).text() )
})

$(function(){
	getContent();
	$('#search').focus();
})

var newDates = [];
for(x=0;x<=20;x++){
	newDates.push( moment().day( 0 + (x *7) ).format('MM/DD/YYYY') );
}
vm.dates(newDates);

editItem = function(item){
	console.log(item);
	vm.seltdItem(item);
}

saveItem = function(){
	if ( !vm.newQuoteDate() ){
		alert('You must set a date to schedule the quote!');
		return false;
	}
	if ( !vm.newQuote() ){
		alert('You must enter a quote!');
		return false;
	}
	var payload = {
		date: vm.newQuoteDate() + ' 00:00',
		type: 'quote',
		subject: 'Quote for ' + vm.newQuoteDate(),
		content: vm.newQuote(),
		meta: ''
	}
	$.post('/api/scheduledContent',payload);
	vm.newQuote('');
	vm.newQuoteDate('');
}

getContent = function(){
	$.get('/api/scheduledContent', {date: moment().day(0).format('YYYY-MM-DD 00:00')}, function(content){
		vm.scheduledContent( content );
	})
}

searchQuote = function(data, event){
	if(event.which == 13){
		$('#search').attr('disabled',true)
		$.get('/api/searchQuotes', {term: vm.quoteSearch()}, function(quotes){
			vm.quotes(quotes);
			$('#search').attr('disabled',false)
		})
	}
}