$('h2').eq(0).before('<div id="TOC"><ul></ul></div>');

$('h2,h3').each(function(){
	var myText = $(this).text();
	var myNewID = 'section-' + $(this).attr('id');
	var myTag = $(this).prop('tagName');
	$(this).before('<a name="'+ myNewID +'"></a>');
	$('#TOC').append('<li class="' + myTag + '"><a href="#'+ myNewID +'">'+ myText +'</a></li>');
})

$('#TOC').prepend('<h2>Table of Contents</h2>');