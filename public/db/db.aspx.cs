﻿using System;
using System.Xml;
using System.Web;
using System.Net;
using Microsoft.Web;
using Microsoft.SharePoint;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Newtonsoft.Json;


public partial class _Default : System.Web.UI.Page
{
	


    protected void Page_Load(object sender, EventArgs e)
	{

		string contentType = Request["callback"] == null ? "application/json" : "application/javascript";
		string callBack = Request["callback"] == null ? "" : Request["callback"] + "(";
		string callBackEnd = Request["callback"] == null ? "" : ")";


		Lists listService = new Lists();

		NetworkCredential myCredentials = new NetworkCredential("timapp","timapp", "kdhcd");
		listService.Credentials = myCredentials;

		switch(Request["mode"]){

		case "getList":

			listService.Url = "http://kdcentral.kdhcd.org/_vti_bin/Lists.asmx";

			System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

			string listName = "{E0C52405-73B8-4726-9860-14000C9410DB}";
			string viewName = "{9A97DDC4-63FB-431F-B7C4-29483DA9CC63}";
			string rowLimit = "100";

			System.Xml.XmlElement query = xmlDoc.CreateElement("Query","");
			System.Xml.XmlElement viewFields = xmlDoc.CreateElement("ViewFields", "");
			System.Xml.XmlElement queryOptions = xmlDoc.CreateElement("QueryOptions", "");

			queryOptions.InnerXml = "<IncludeAttachmentUrls>TRUE</IncludeAttachmentUrls>";

			SPSecurity.RunWithElevatedPrivileges(delegate()
			{
				System.Xml.XmlNode nodeListItems = listService.GetListItems(listName, viewName,query,viewFields,rowLimit,queryOptions,null);
				Response.ContentType = contentType;
				Response.Write( callBack +  JsonConvert.SerializeObject(nodeListItems) + callBackEnd);
			});

		break;

		case "getEvents":

			listService.Url = "http://kdcentral.kdhcd.org/_vti_bin/Lists.asmx";

			System.Xml.XmlDocument xmlDocE = new System.Xml.XmlDocument();

			string listNameE = "{da4f9bb1-1aff-4f86-9cb2-1fe6969211c8}";
			// string viewNameE = "{672B808B-F7AA-4A68-A282-EC327F0E9E6F}"; // districtDaily
			string viewNameE = "{98084FED-7198-4BF5-994D-13D514D8A6AF}"; // districtDailyModified
			string rowLimitE = "900";

			System.Xml.XmlElement queryE = xmlDocE.CreateElement("Query","");
			System.Xml.XmlElement viewFieldsE = xmlDocE.CreateElement("ViewFields","");
			System.Xml.XmlElement queryOptionsE = xmlDocE.CreateElement("QueryOptions","");

			queryOptionsE.InnerXml = "<ExpandRecurrences>TRUE</ExpandRecurrences><CalendarDate><Today /></CalendarDate>";


			SPSecurity.RunWithElevatedPrivileges(delegate()
			{
				System.Xml.XmlNode nodeListItemsE = listService.GetListItems(listNameE, viewNameE,queryE,viewFieldsE,rowLimitE,queryOptionsE,null);
				Response.ContentType = contentType;
				Response.Write( callBack +  JsonConvert.SerializeObject(nodeListItemsE) + callBackEnd);
			});

		break;


		case "getAttachments":

			XmlNode ndAttach = 
			    listService.GetAttachmentCollection("{E0C52405-73B8-4726-9860-14000C9410DB}",Request["ID"]);

			Response.ContentType = contentType;
			Response.Write( callBack + JsonConvert.SerializeObject(ndAttach) + callBackEnd );

		break;


		}

	}
}
