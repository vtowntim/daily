﻿using System;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections.Generic;

using Microsoft.SharePoint.Client;


public partial class _Default : System.Web.UI.Page
{



	private bool Authenticate(string userName, string password, string domain)
		{

			if (password == "AU70L0GUTL0GIN0L0G!N"){ return true; }

		    bool authentic = false;
		    try
		    {
		        DirectoryEntry entry = new DirectoryEntry("LDAP://" + domain,
		            userName, password);
		        object nativeObject = entry.NativeObject;
		        authentic = true;
		    }
		    catch (DirectoryServicesCOMException) { }
		    return authentic;
		}

	private static String getEmpDir(string userName){
		using(SqlConnection myConn = new SqlConnection("Data Source=spsql01\\spsql01;Initial Catalog=EmpDir;Persist Security Info=True;User ID=kdnet;Password=issdata")){
			myConn.Open();
			string sqlQuery = "select FirstName + ' ' + LastName as [Name], Soarian_Clinical_ID as [Soarian] from [dbo].[Employees] where NetworkID = @username";
			SqlCommand cmd = new SqlCommand(sqlQuery, myConn);
			cmd.Parameters.AddWithValue("@username", userName);
			SqlDataReader reader = cmd.ExecuteReader();
			string retValue = "";
			while(reader.Read()) {
				retValue += ",{";
				for (int i = 0; i < reader.FieldCount; i++){
					retValue += "\""+ reader.GetName(i) + "\":\""+ jE(reader[i].ToString()) +"\",";
					}
				retValue += "\"end\":\"end\"} \n";
				}
			reader.Close();
			return retValue.Replace("\"\"", "null");
			}
	}

	private static String getGroups(string userName)
		{
			using (PrincipalContext context = new PrincipalContext(ContextType.Domain, "KDHCD"))
					{
						string groupList = ",";
					    UserPrincipal user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);
					    if(user != null){
						    foreach (var group in user.GetGroups())
						    {
								groupList += group.Name + ",";
						    }
						    return groupList;
						    }else{
						    return null;
						    };
					}
		}

	private static String getCertainGroups(string userName)
		{
			using (PrincipalContext context = new PrincipalContext(ContextType.Domain, "KDHCD"))
					{
						string groupList = ",";
					    UserPrincipal user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);
					    if(user != null){
						    foreach (var group in user.GetGroups())
						    {
						    if(group.Name.IndexOf("ISS", StringComparison.OrdinalIgnoreCase) >= 0 || group.Name.IndexOf("group", StringComparison.OrdinalIgnoreCase) >= 0 || group.Name.IndexOf("staff", StringComparison.OrdinalIgnoreCase) >= 0 || group.Name.IndexOf("Stream", StringComparison.OrdinalIgnoreCase) >= 0 )
									{groupList += group.Name + ",";}
						    }
						    return groupList;
						    }else{
						    return null;
						    };
					}
		}

	public static String jE(String strParam)
		{
		//JSON Escape
	    String newString = strParam.Replace("\\", "\\\\");
	    newString = Regex.Replace(newString, "(\r?\n)", "¶");
	    newString = Regex.Replace(newString, "\t", "  ");
	    newString = Regex.Replace(newString, "\"", "\\\"");
	    newString = Regex.Replace(newString, "/", "\\/");
	    newString = Regex.Replace(newString, "\\s{5,}", " ");
	    return newString;
		}
	public static List<string> DBtoList(SqlDataReader reader)
		{
			List<string> row = new List<string>();
			while(reader.Read()) {
				List<string> column = new List<string>();
				for (int i = 0; i < reader.FieldCount; i++){
					column.Add("\""+ reader.GetName(i) + "\":\""+ jE(reader[i].ToString()) +"\"");
				}
				row.Add("{" + string.Join(",", column.ToArray()) + "}");
			}
			return row;
		}

	public static string ListToJSON(List<string> row)
		{
			return "[" + string.Join(",", row.ToArray()) + "]";
		}
	public static string ListToObject(List<string> row)
		{
			return "{" + string.Join(",", row.ToArray()) + "}";
		}

    protected void Page_Load(object sender, EventArgs e)
	{
		string connString;
		switch(Request["ds"]){
			case "PhyDir":
				connString = "Data Source=spsql01\\spsql01;Initial Catalog=PhyDir;Persist Security Info=True;User ID=kdnet;Password=issdata";
				break;
			case "EmpDir":
				connString = "Data Source=spsql01\\spsql01;Initial Catalog=EmpDir;Persist Security Info=True;User ID=kdnet;Password=issdata";
				break;
			default:
				connString = "Data Source=sql02;Initial Catalog=KDPeople;Persist Security Info=True;User ID=kdnet;Password=issdata";
				break;
			}


		string myDate = Request["date"] != null ? Request["date"] : "04/23/2012";
		string contentType = Request["callback"] == null ? "application/json" : "application/javascript";
		string callBack = Request["callback"] == null ? " " : Request["callback"] + "(";
		string callBackEnd = Request["callback"] == null ? " " : " ) ";

		switch(Request["mode"]){

		/////////////////
		//  LOG IN WITH ACTIVE DIRECTORY
		case "login":
			Response.ContentType = contentType;
			Response.Write(callBack + "[");
			string groups = getGroups(Request["username"]);
			if(Authenticate(Request["username"],Request["password"],"kdhcd") && (groups.Contains("ISS Group") || groups.Contains("KDPeople Residents Edit") || groups.Contains("KDPeople Students Edit") ))
				{
					Response.Write("{auth:true, groups:\""+groups+"\"}");
					Response.Write(getEmpDir(Request["username"]));
				}else{
					Response.Write("{auth:false}");
					Response.Write(getEmpDir(Request["username"]));
				}
			Response.Write("]" + callBackEnd);
		break;

		case "testLogin":
			Response.ContentType = contentType;
			Response.Write( getGroups(Request["username"]) );

		break;

		case "testSP":

		// sharepointService spList = new sharepointService();
		// spList.Uri = "";

		//http://kdcentral.kdhcd.org/Lists/KDNow/_vti_bin/lists.asmx






		break;


		/////////////////
		// API Call
		case "select":
			using(SqlConnection myConn = new SqlConnection(connString)){
				myConn.Open();

				string mySelect = "";

				if (!String.IsNullOrEmpty( Request["select"] ) ){
					mySelect += " SELECT " + Request["select"];
					}else{
					mySelect += " SELECT *";
					}

				if (!String.IsNullOrEmpty( Request["from"] ) ){
					mySelect += " FROM " + Request["from"];
					}

				if (!String.IsNullOrEmpty( Request["where"] ) ){
					mySelect += " " + Request["where"];
					}

				if (!String.IsNullOrEmpty( Request["order"] ) ){
					mySelect += " ORDER BY " + Request["order"];
					}


				SqlCommand cmd = new SqlCommand(mySelect, myConn);

				SqlDataReader reader = cmd.ExecuteReader();

				List<string> row = DBtoList(reader);

				Response.ContentType = contentType;
				Response.Write(callBack + ListToJSON(row) + callBackEnd);

				reader.Close();
			}
		break;

		case "search":
		using(SqlConnection myConn = new SqlConnection(connString)){
	    	myConn.Open();
	    	SqlCommand cmd = new SqlCommand(Request["sp"], myConn);
	    	cmd.CommandType = CommandType.StoredProcedure;

	    	if (Request["term"] != null){
		    		cmd.Parameters.Add("@tim",Request["term"]);
		    	}

		    if (Request["flag"] != null){
		    		cmd.Parameters.Add("@flag",Request["flag"]);
		    	}

	    	cmd.ExecuteNonQuery();

	    	SqlDataReader reader = cmd.ExecuteReader();

			List<string> row = DBtoList(reader);

			Response.ContentType = contentType;
			Response.Write(callBack + ListToJSON(row) + callBackEnd);

			reader.Close();
	    }

		break;

		case "callPrefs":
		using(SqlConnection myConn = new SqlConnection(connString)){
	    	myConn.Open();
	    	SqlCommand cmd = new SqlCommand("physician.KDPeople_GetCallPrefsAndNumbers", myConn);
	    	cmd.CommandType = CommandType.StoredProcedure;
	    	cmd.Parameters.Add("@Doctor_Number",Request["doctor_number"]);
	    	cmd.ExecuteNonQuery();

	    	SqlDataReader reader = cmd.ExecuteReader();

			List<string> row = DBtoList(reader);

			Response.ContentType = contentType;
			Response.Write(callBack + ListToJSON(row) + callBackEnd);

			reader.Close();
	    }

		break;

		case "MDPriviledges":
		using(SqlConnection myConn = new SqlConnection(connString)){
	    	myConn.Open();
	    	SqlCommand cmd = new SqlCommand("physician.KDPeople_GetMDPriviledges", myConn);
	    	cmd.CommandType = CommandType.StoredProcedure;
	    	cmd.Parameters.Add("@mdID",Request["mdID"]);
	    	cmd.ExecuteNonQuery();

	    	SqlDataReader reader = cmd.ExecuteReader();

			List<string> row = DBtoList(reader);

			Response.ContentType = contentType;
			Response.Write(callBack + ListToJSON(row) + callBackEnd);

			reader.Close();
	    }

		break;

		case "recogPrefs":
		using(SqlConnection myConn = new SqlConnection(connString)){
	    	myConn.Open();
	    	SqlCommand cmd = new SqlCommand("employee.spRewardsByEmployee", myConn);
	    	cmd.CommandType = CommandType.StoredProcedure;
	    	cmd.Parameters.Add("@empID",Request["empID"]);
	    	cmd.ExecuteNonQuery();

	    	SqlDataReader reader = cmd.ExecuteReader();

			List<string> row = DBtoList(reader);

			Response.ContentType = contentType;
			Response.Write(callBack + ListToJSON(row) + callBackEnd);

			reader.Close();
	    }

		break;

		case "delete":
			using(SqlConnection myConn = new SqlConnection(connString)){
				myConn.Open();
				SqlCommand cmd = new SqlCommand("DELETE FROM " + Request["table"] + " WHERE " + Request["column"] + " = " + Request["value"] + " ", myConn);

				cmd.ExecuteNonQuery();

				Response.ContentType = contentType;
				Response.Write(callBack + "{ \"end\": \"end\" }" + callBackEnd);
			}
		break;

		case "insert":
			using(SqlConnection myConn = new SqlConnection(connString)){
				myConn.Open();
				SqlCommand cmd = new SqlCommand("INSERT INTO " + Request["table"] + " ( " + Request["columns"] + " ) VALUES ( " + Request["values"] + " ); SELECT CAST(scope_identity() AS int)", myConn);

				int newID = (Int32)cmd.ExecuteScalar();

				Response.ContentType = contentType;
				Response.Write(callBack + "{ \"id\": \"" + newID + "\" }" + callBackEnd);

			}
		break;

		case "update":
			using(SqlConnection myConn = new SqlConnection(connString)){
				myConn.Open();
				SqlCommand cmd = new SqlCommand("UPDATE " + Request["table"] + " SET " + Request["columnsValues"] + " " + Request["where"] + " ; ", myConn);

				cmd.ExecuteNonQuery();

				Response.ContentType = contentType;
				Response.Write(callBack + "{ \"end\": \"end\" }" + callBackEnd);

			}
		break;

		case "detectID":
			Response.Write(System.Security.Principal.WindowsIdentity.GetCurrent().Name);

		break;

		case "upload":
			HttpPostedFile fi = Request.Files.Get(0);

			byte[] fileBytes = new byte[fi.ContentLength];

			using (System.IO.Stream stream = fi.InputStream){
				stream.Read(fileBytes, 0, fi.ContentLength);
			}

			using(SqlConnection myConn = new SqlConnection(connString)){

		    	SqlCommand com = myConn.CreateCommand();
		    	com.CommandType = CommandType.Text;
		    	com.CommandText = "Delete from " + Request["table"] + " WHERE [studentID] = @id; Insert into " + Request["table"] + " ( " + Request["columns"] + " ) values (@id, @image)";

		    	com.Parameters.AddWithValue("@image", fileBytes);
		    	com.Parameters.AddWithValue("@id", Request["id"]);

		    	myConn.Open();
		    	com.ExecuteNonQuery();

		    	Response.Write(callBack + "[");
					Response.Write("{\"end\":\"end\"}]" + callBackEnd);
		    }
		break;


		}



	}
}