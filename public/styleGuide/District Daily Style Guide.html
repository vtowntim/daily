<!DOCTYPE>
<html>
<head>
	<meta charset="utf-8">
	<meta name="format" content="complete">
</head>
<body>

<style>

body{
	font-family: 'Avenir Next';
	font-size: 12pt;
	width: 35em;
	margin: 1em auto;
}

</style>

<h1>The District Daily Style and Content Guide</h1>

<p>The District Daily is a communication tool that captures emails broadcast to *<em>Everyone</em> and transforms them into a headline list linked to individual posts. Because this is a paradigm change, this style and content guide has been created to assist Content Providers in making informed choices when submitting posts.</p>

<hr>

<h2>Terminology</h2>

<p>The following are definitions to terms that will be referenced throughout this document:</p>

<ul>
<li><strong>Post</strong> — A communication document submitted to be published by the District Daily. A post contains at minimum, a <em>Headline</em> and <em>Content</em>:

<ul>
<li><strong>Headline</strong> — The title of a post that succinctly conveys basic information and entices readers to click-through and view the post&#8217;s content.</li>
<li><strong>Content</strong> — The body of a post which may contain more detailed information than the headline. When a headline is clicked in the District Daily, readers are linked to read the content in a web browser. Content may contain images and attachments, and is not limited in size.</li>
</ul></li>
<li><strong>Content Provider</strong> — Individuals who will submit posts (<em>via Email or otherwise</em>) to the District Daily.</li>
<li><strong>Readers</strong> — Individuals including all district employees who receive the District Daily.</li>
</ul>

<hr>

<h2>Posts</h2>

<h3>Know Your Audience</h3>

<p>In the past, *<em>Everyone</em> emails went to&#8230; <em>well,</em> everyone and Content Providers didn&#8217;t think much about the global relevancy of their messages. With the District Daily, we&#8217;re asking everyone to think twice before posting anything at all. A click-tracking feature of the District Daily shows you how people actually read your message, allowing you to make better data-driven decisions about your posts. This isn&#8217;t an effort to discourage communication, but to help make it more meaningful.</p>

<p>Ask yourself:</p>

<ul>
<li><strong>Does my message only apply to a certain department or smaller audience?</strong> If so, maybe a district-wide post isn&#8217;t the appropriate medium.</li>
<li><strong>Does my message convey useful information?</strong> For instance, are you just telling us that your ancillary, non-patient-care department is closed for Thanksgiving?</li>
</ul>

<hr>

<h2>Headlines</h2>

<h3>Subject Lines Become Headlines</h3>

<p>In the District Daily, your email subject lines become <em>Headlines</em> that would either entice or discourage readers from clicking through to your content. Before you hit “<em>send</em>,” take a moment to write a subject line that accurately and concisely describes your content, giving readers a concrete reason to engage with your message. </p>

<h3>7 Guidelines for Composing Engaging District Daily Headlines:</h3>

<ol>
<li><strong>Keep it very simple and understandable.</strong> Do not use abbreviations, technical jargon, or hard to understand words. A well-written headline and content, should be written at a grade 7 reading level.</li>
<li><strong>Be precise.</strong> Avoid writing <em>&#8216;Blind Headlines&#8217;</em> that reveal nothing about your content. A headline must stand entirely on it&#8217;s own merit, without the reader being forced to read on to discover what the headline was meaning. They won&#8217;t read on.</li>
<li><strong>Tell more; Sell more.</strong> Headlines containing 10 or more words with factual or news worthy information, outsell short headlines and will be more successful at attracting clicks-throughs.</li>
<li><strong>Keep it between 90 and 150 characters.</strong> Keep your headlines short and concise. Don’t write convoluted copy that will take your visitor a long time to read. Studies have shown that the most engaging headlines are between 90 and 150 characters.</li>
<li><p><strong>Begin with <em>Subjects</em> and <em>Verbs</em>.</strong> In the English language, we read from left to right, and verbs and subjects help us to quickly glean the meaning of a sentence. </p>

<blockquote>
<p><em>Example: &#8216;Join us at the Kaweah Delta Talent Show - Wednesday, March 26th at 7pm&#8217;</em></p>
</blockquote></li>
<li><strong>Do not use <em>ALL CAPITAL LETTERS</em>.</strong> Though you may think you are emphasizing your headline, it comes across as yelling. Also, because readers have learned to recognize the <em>&#8216;shape&#8217;</em> of words, a skimming reader may miss your headline altogether.</li>
<li><p><strong>Use numbers when possible.</strong> Using data and numbers is a great way to demonstrate that your message is clear and straightforward. Numbers break through the clutter of ambiguity, adds specificity to your offer, and sets the clear expectations.</p>

<blockquote>
<p><em>Example: &#8216;The Nursing Banquet is 6.5 weeks away! Work with your Team to Donate a Basket!&#8217;</em></p>
</blockquote></li>
</ol>

<h3>Do You Need More Than a Headline?</h3>

<p>Many posts that end up in the District Daily really don&#8217;t need anything more than an informative, clear headline. For instance, if you are just wanting to remind readers of an event you already posted about, <em>explicitly include the relevant information in your headline</em>.</p>

<blockquote>
<p><strong>Example:</strong></p>

<ul>
<li><em>Support Services Building: Power Outage this Friday, 7am to 8:45am</em></li>
<li><em>Kaweah Korner will be Closed Early Monday and Tuesday at 12pm</em></li>
<li><em>Remember to Wear Pink on Friday to Support Breast Cancer Awareness!</em></li>
</ul>
</blockquote>

<h3>Title-Casing</h3>

<p>A good rule of thumb for District Daily headlines is to use Title-Casing; Capitalize the first word of the headline, the last word, and all major words (<em>usually more than 4 letters</em>) in between.</p>

<blockquote>
<p><strong>Correct:</strong></p>

<ul>
<li><em>Support Services Building: Power Outage this Friday, 7am to 8:45am</em></li>
<li><em>Kaweah Korner will be Closed Early Monday and Tuesday at 12pm</em></li>
<li><em>Remember to Wear Pink on Friday to Support Breast Cancer Awareness!</em></li>
</ul>

<p><strong>Incorrect:</strong> </p>

<ul>
<li><em>Support services building: power outage this friday, 7am to 8:45am</em></li>
<li><em>Kaweah Korner Will Be Closed Early Monday And Tuesday At 12pm</em></li>
<li><em>remember to wear pink on Friday to support Breast Cancer</em></li>
</ul>
</blockquote>

<hr>

<h2>Content</h2>

<h3>Not an Email, a Web Page</h3>

<p>The body of emails submitted to the District Daily are transformed into individual web pages linked from Headlines. Please recognize that web pages and emails have very different contexts and should be treated as separate mediums.</p>

<h3>Email Patterns to Avoid</h3>

<p>Failure to avoid the following patterns may cause readers to perceive your content to be out-of-place or irrelevant.</p>

<ul>
<li><strong>Remove your signature.</strong> All posts published by the District Daily have an informative <em>By-line</em> linking to your profile. Email signatures are redundant and generally take up more space than needed. If your content requires follow-up communication from readers, consider adding a &#8216;<em>call-to-action</em>&#8217; within your content explicitly prescribing your preferred contact method.</li>
<li><strong>Remove forwarding and replying headers.</strong> If your content is the last thread of email replies or forwards, clean it up and give readers only the information they need.</li>
<li><strong>Remove confidentiality disclaimers.</strong> Aside from the fact that these disclaimers are <a href="http://www.economist.com/node/18529895">pointless in even emails</a>, in the context of published web pages, they are nonsensical.</li>
</ul>

<h3>Guidelines for Writing Worthwhile Content</h3>

<ul>
<li><strong>Start off on a good foot.</strong> The opening paragraph, called the &#8216;<em>lede</em>&#8217; in journalism, is often considered the most important part of an article, for the same reasons a headline is so important: if the lede isn’t good, the reader will skip the rest of the article. Get the readers&#8217; attention without being too sensational, and explain <em>why</em> they should keep reading.</li>
<li><strong>Write short paragraphs, separated by blank lines.</strong> Most people find unbroken blocks of text boring, or even intimidating. Take the time to format your message for the ease of your reader.</li>
<li><strong>Use bullets.</strong> The most effective way to present information and keep readers from leaving is to make sure that everything is compact and easy to read.

<ul>
<li>Using bullets will help you do that.</li>
<li>They eliminate the need for too many unnecessary words.</li>
<li>Bullets are scannable.</li>
<li>Readers who are scanning a page will easily consume the content.</li>
</ul></li>
<li><strong>Limit paragraphs to less than 5 sentences.</strong> Most people don’t like reading a lengthy post. Avoid fluff and resist the urge to digress. Paragraphs that are less than 5 sentences are also easier to comprehend and readers will find it easier to recall your message when less words were used. <em>Less is more</em>.</li>
<li><strong>Bold your main points.</strong> If you want to emphasize a point in the middle of a longer paragraph, <strong>you can make it bold</strong>. Don’t do this too often or readers will tire of it.</li>
<li><strong>Include Images</strong> Readers are visual learners and images can help people take in and retain information better. Referencing a flier? Include it as an image.</li>
<li><strong>Write in a conversational style.</strong> There is a road sign often posted near construction sites that reads, &#8220;<em>Maintain present lane.</em>&#8221; Why so formal? A more conversational style would be better: &#8220;<em>Stay in your lane</em>&#8221; or &#8220;<em>Do not change lanes.</em>&#8221; If you write as if you’re wearing a top hat and spats, you distance yourself from the reader and muddle the message.</li>
</ul>

</body>
</html>