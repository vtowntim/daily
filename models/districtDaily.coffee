insertList = (date, content, callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	moment = require("moment")

	return false    if not date or not content

	if typeof content is "object"
		contentObject = JSON.stringify(content)
	else
		contentObject = content

	console.log "Inserting content for"
	console.log moment(date).toDate()

	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "date", sql.DateTime, moment(date).toDate()
		request.input "contentObject", sql.Text, contentObject
		request.execute "districtDailyInsertList", (err, recordsets, returnValue) ->
			callback err    if callback
			console.log "Inserted/Updated List for " + date
			return

		return
	)
	return

exports.insertList = insertList




getList = (date, callback) ->
	console.log "getList..."

	sqlUtil = require("../models/sqlUtil")

	sqlUtil.simpleSql "select top 1 * from [districtDaily-Lists] where convert(date, [date]) = convert(date,'" + date + "') order by date desc", (err, recordset) ->
		console.log arguments
		if err
			console.error err
		else
			if recordset
				contentObject = (if recordset.length then JSON.parse(recordset[0].contentObject) else null)
				callback err, contentObject
			else
				callback err, null
		return

	return

exports.getList = getList





gatherContent = (day, callback) ->
	async = require("async")
	content = require("../models/content")
	events = require("../models/events")
	moment = require("moment")
	weather = require("../models/weather")
	reports = require("../models/reports")
	kdPeople = require("../models/kdPeople")
	kaweahCare = require("../models/kaweahCare")
	us = require("underscore")

	day = day or new Date()
	today = moment(day).hour(10).minute(0).second(0).format("YYYY-MM-DD HH:mm:ss")
	today2 = moment(day).hour(10).minute(0).second(0)

	console.log "making content for"
	console.log today

	processes =
		records: (callback) ->
			content.makeRecordsList today2, (err, records) ->
				callback err, records
				return

			return

		focus: (callback) ->
			content.getFocusByDay today2, (err, focusData) ->
				callback err, focusData[0][0]
				return

			return

		weather: (callback) ->
			weather.getForecast (err, weather) ->
				callback err, weather
				return

			return

		featuredEvents: (callback) ->
			events.makeFeaturedEventsList (err, events) ->
				callback err, events
				return
			return

		events: (callback) ->
			events.makeEventsList (err, events) ->
				callback err, events
				return

			return

		recognitions: (callback) ->
			kaweahCare.getRecognition (err, recognizedEmployees) ->
				callback err, recognizedEmployees
				return

			return

		anniversaries: (callback) ->
			kdPeople.getAnniversaries today, (err, rows) ->
				annInfo = us.groupBy(rows[0], "years")
				annYears = us.keys(annInfo).reverse()
				callback err,
					years: annYears
					data: annInfo

				return

			return

		count: (callback) ->
			reports.count (err, count) ->
				callback err, count[0][0]
				return

			return

	async.parallel processes, (err, content) ->
		content.listDay = moment(today).format("YYYY-MM-DD HH:mm")
		content.title = "District Daily for " + moment(today).format("MMMM Do, YYYY")
		insertList today, content
		callback err, content
		return

	return

exports.gatherContent = gatherContent





build = (callback) ->
	# Downloads content from SharePoint
	async = require("async")
	content = require("../models/content")
	events = require("../models/events")

	output = ""

	processes = [
		(callback) ->
			content.getContentFromSharePoint (err, res) ->
				output += "Content loaded from SharePoint.\n"
				callback err, true
				return

		(callback) ->
			events.getEventsFromSharePoint (err, res) ->
				output += "Events loaded from SharePoint.\n"
				callback err, true
				return

	]

	async.parallel processes, (err, results) ->
		
		#console.log(output);
		callback err, output    if callback
		return

	return

exports.build = build





getPost = (post, callback) ->
	async = require("async")
	content = require("../models/content")

	processes =
		recordset: (callback) ->
			content.getPost post, callback
			return

		attachments: (callback) ->
			content.getAttachments post, callback
			return

	async.parallel processes, callback
	return

exports.getPost = getPost





getFocus = (focusID, callback) ->
	async = require("async")
	content = require("../models/content")
	processes = recordset: (callback) ->
		content.getFocus focusID, callback
		return

	async.parallel processes, callback
	return

exports.getFocus = getFocus





getEvent = (event, occurrence, callback) ->
	async = require("async")
	events = require("../models/events")
	processes = recordset: (callback) ->
		events.getEvent event, occurrence, callback
		return

	async.parallel processes, callback
	return

exports.getEvent = getEvent





announcement = (httpResponse, thisIsATest, callback) ->
	md = require("marked")
	content =
		title: "Announcing the District Daily"
		md: md

	httpResponse.render "mail/announcement", content, (err, html) ->
		sendTheDaily html, httpResponse, thisIsATest, "Announcing the District Daily!"
		callback err, html  if callback
		return

	return

exports.announcement = announcement




removeDeletedEvents = (content)->

	content


modifyContent = (content)->
	console.log "Content is modified!"
	content = removeDeletedEvents content

	return content
exports.modifyContent = modifyContent






prepareAndSendTheDaily = (httpResponse, thisIsATest, callback) ->
	gatherContent null, (err, content) ->
		if err
			console.error err
		else

			content = modifyContent content

			httpResponse.render "mail/mailList", content, (err, html) ->
				sendTheDaily html, httpResponse, thisIsATest
				callback err, html  if callback
				return

		return

	return

exports.prepareAndSendTheDaily = prepareAndSendTheDaily





blastTheDailyEmail = (addresses, emailBodyHTML, httpResponse, subject) ->
	mail = require("../models/mail")
	mailOptions =
		from: "District Daily <DistrictDaily@kdhcd.org>"
		to: addresses
		subject: subject
		html: emailBodyHTML
		forceEmbeddedImages: false

	
	#generateTextFromHTML: true
	httpResponse.send "sent!"
	mail.blast mailOptions
	return

exports.blastTheDailyEmail = blastTheDailyEmail





sendTheDaily = (emailBodyHTML, httpResponse, thisIsATest, subject) ->
	moment = require("moment")
	kdPeople = require("../models/kdPeople")
	subject = subject or "District Daily - " + moment().format("dddd, MMMM Do, YYYY")
	if thisIsATest
		console.log "Sending the District Daily using Test Addresses."
	else
		console.log "Sending the District Daily"
	if thisIsATest
		console.log "This is a Test!"
		
		# var addresses = thisIsATest ? ['tanderso','dvolosin','ddesimas','bcapwell'] : [ 'tanderso', 'cmajor', 'ldryden', 'MSutherl', 'slarkin', 'cstanley', 'ddesimas', 'dvolosin', 'dquesnoy',  'bcapwell', 'mblanken','dcochran','tmendenh','wheather','kbentson','rlauck','lledesma','rlizardo','emiller','alrodrig','vturner','lflorez','GAhTye','baguilar','LSchultz','CLaBlue','CLawry','JMcNulty','dlmyers','TRayner','JRamirez','JStockto','jtyndal' ];
		addresses = (if typeof thisIsATest is "object" then thisIsATest else [ #'tanderso';
			"tanderso"
			"dvolosin"
			"ddesimas"
			"bcapwell"
		])
		blastTheDailyEmail addresses, emailBodyHTML, httpResponse, subject
	else
		
		#BETA GROUP:
		# var addresses = [ 'tanderso', 'cmajor', 'ldryden', 'MSutherl', 'slarkin', 'cstanley', 'ddesimas', 'dvolosin', 'dquesnoy',  'bcapwell', 'mblanken','dcochran','tmendenh','wheather','kbentson','rlauck','lledesma','rlizardo','emiller','alrodrig','vturner','lflorez','GAhTye','baguilar','LSchultz','CLaBlue','CLawry','JMcNulty','dlmyers','TRayner','JRamirez','JStockto','jtyndal' ];
		# blastTheDailyEmail(addresses, emailBodyHTML, httpResponse, subject);
		
		# Production Group:
		kdPeople.getEmployeeUsernames (err, usernames) ->
			console.error err   if err
			if usernames
				console.log "We have usernames!"
				blastTheDailyEmail usernames, emailBodyHTML, httpResponse, subject
			return

	return

exports.sendTheDaily = sendTheDaily




