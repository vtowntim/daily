var sqlUtil = require('../models/sqlUtil');
var utils = require('../models/utils');

var makeHit = function(id, type, user, dontCount) {

    sqlUtil.simpleSql("insert into [dbo].[districtDaily-Hits] values ('" + id + "', '" + type + "', '" + user + "', getdate())", function(err, data) {
        if (err) {
            console.error(err);
        } else {
            if (!dontCount) countHits(id, type);
        }
    })

}
exports.makeHit = makeHit;

var quickHit = function(type, req){
    var utils = require('../models/utils');
    userIP = utils.getClientAddress(req);

    makeHit('0', 'StyleGuide', userIP, true);
}
exports.quickHit = quickHit;

var countDetail = function(id, type, callback){
    sqlUtil.simpleSql("select min(date) as [date], [user] from [dbo].[districtDaily-Hits]  where id= " + id + " and type= '" + type + "' group by [user]", function(err, data){
        callback(err, data);
    });
}
exports.countDetail = countDetail;

var getCounts = function(id, type, callback) {
    sqlUtil.simpleSql("select count(DISTINCT [user]) as [hits] from [dbo].[districtDaily-Hits]  where id= " + id + " and type= '" + type + "'", function(err, data) {
        updateCounts(data[0].hits, id, type, callback);
    });
}
exports.getCounts = getCounts;



var updateCounts = function(hits, id, type, callback) {
    sqlUtil.simpleSql("update [dbo].[districtDaily-" + type + "] set hits = " + hits + "  where spId= " + id, callback);
}
exports.updateCounts = updateCounts;



var countHits = function(id, type) {

    getCounts(id, type, function(err, data) {
        if (err) {
            console.error(err);
        }
    });

}
exports.countHits = countHits;