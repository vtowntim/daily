exports.getRecognition = (callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	us = require("underscore")
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "days", sql.Int, 7
		request.execute "districtDaily-getRecognizedPeople", (err, data) ->
			callback err, data[0]
			return

		return
	)
	return


exports.getEOM = (callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	us = require("underscore")
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "days", sql.Int, 7
		request.execute "districtDaily-getEOM", (err, data) ->
			callback err, data[0]
			return

		return
	)
	return



exports.insertEOM = (recognitionObj, callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	unless recognitionObj.transcribed is true
		mail = require("../models/mail")
		mail.sendMail
			to: recognitionObj.supervisor + "@kdhcd.org"
			cc: recognitionObj.employee + "@kdhcd.org, " + recognitionObj.sender + "@kdhcd.org"
			from: recognitionObj.sender + "@kdhcd.org"
			subject: recognitionObj.recognizee + " has received a SERVICE EXCELLENCE AWARD NOMINATION!"
			text: recognitionObj.description

	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.verbose = true
		request.input "id", sql.Int, recognitionObj.id
		request.input "recognized", sql.VarChar, recognitionObj.recognized
		request.input "employee", sql.VarChar, recognitionObj.employee
		request.input "supervisor", sql.VarChar, recognitionObj.supervisor
		request.input "sender", sql.VarChar, recognitionObj.sender
		request.input "description", sql.VarChar, recognitionObj.description
		request.input "transcribedBy", sql.VarChar, recognitionObj.transcribedBy
		request.execute "districtDailyInsertEOM", callback
		return
	)
	return




exports.insertRecognition = (recognitionObj, callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	unless recognitionObj.transcribed is true

		if recognitionObj.type is "team"

			mail = require("../models/mail")
			mail.sendMail
				to: "elargoza@kdhcd.org"
				cc: "#{recognitionObj.sender}@kdhcd.org"
				# to: 'vtowntim@gmail.com'
				from: "#{recognitionObj.sender}@kdhcd.org"
				subject: "DREAM TEAM NOMINATION"
				text: recognitionObj.description

		if recognitionObj.type is "physician"

			mail = require("../models/mail")
			mail.sendMail
				to: "#{recognitionObj.supervisor}@kdhcd.org"
				cc: "#{recognitionObj.email}, #{recognitionObj.sender}@kdhcd.org"
				# to: 'vtowntim@gmail.com'
				from: "#{recognitionObj.sender}@kdhcd.org"
				subject: "#{recognitionObj.recognizee} has received a KAWEAH CARE RECOGNITION!"
				text: recognitionObj.description

		if recognitionObj.type is "employee"

			mail = require("../models/mail")
			mail.sendMail
				to: "#{recognitionObj.supervisor}@kdhcd.org"
				cc: "#{recognitionObj.employee}@kdhcd.org, #{recognitionObj.sender}@kdhcd.org"
				from: "#{recognitionObj.sender}@kdhcd.org"
				subject: "#{recognitionObj.recognizee} has received a KAWEAH CARE RECOGNITION!"
				text: recognitionObj.description

	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.verbose = true
		request.input "id", sql.Int, recognitionObj.id
		request.input "recognized", sql.VarChar, recognitionObj.recognized
		request.input "employee", sql.VarChar, recognitionObj.employee or recognitionObj.type
		request.input "supervisor", sql.VarChar, recognitionObj.supervisor or recognitionObj.type
		request.input "sender", sql.VarChar, recognitionObj.sender
		request.input "description", sql.VarChar, recognitionObj.description
		request.input "transcribedBy", sql.VarChar, recognitionObj.transcribedBy
		request.input "type", sql.VarChar, recognitionObj.type or "employee"
		request.execute "districtDailyInsertRecognition", callback
		return
	)
	return
