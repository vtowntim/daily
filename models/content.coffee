insertContent = (row, callback) ->
	sql = require("mssql")
	moment = require("moment")
	attachments = require("../models/attachments")
	sqlUtil = require("../models/sqlUtil")
	utils = require("../models/utils")

	return false  unless row

	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)

		attachments.processAttachments row["@ows_ID"], row["@ows_Attachments"].split(";#")

		request.input "spId", sql.Int, row["@ows_ID"]
		request.input "body", sql.Text, utils.cleanHTML(row["@ows_Body"])
		request.input "creator", sql.VarChar, row["@ows_EmailFrom"].match(/(.*) \</)[1]
		request.input "username", sql.VarChar, row["@ows_EmailFrom"].match(/<(.*)\@/)[1]
		createdDate = row["@ows_RunDateTime"] or row["@ows_Created"]
		request.input "created", sql.VarChar, moment(createdDate).format("YYYY-MM-DD HH:mm:ss")

		console.log "----", row["@ows_Title"]
		console.log moment(createdDate).utc().toDate()

		rowTitle = row["@ows_Title"] or "untitled"
		rowTitle += "#hideFromDD"  if row["@ows_HideFromDD"] is "1"

		request.input "title", sql.VarChar, rowTitle.replace(/\s?(\#(\w*)|^RE:|^FW:|^Recall:)\s?/g, "")

		tags = rowTitle.match(/\#(\w*)/g, "")
		tags = (if tags then tags.join() else null)

		request.input "tags", sql.VarChar, tags

		request.execute "districtDailyInsertContent", (err, recordsets, returnValue) ->
			callback err
			console.error err  if err
			console.log "Inserted/Updated #" + row["@ows_ID"]
			return

		return
	)
	return
exports.insertContent = insertContent





getFocusByDay = (day, callback) ->
	moment = require("moment")
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")

	myDate = moment(day).toDate() #.format('YYYY-MM-DD HH:mm:ss')

	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)

		request.input "searchDate", sql.DateTime, myDate

		unless callback
			callback = ->
		request.execute "districtDaily-getFocus", callback
		return
	)
	return
exports.getFocusByDay = getFocusByDay




getContentByUsername = (username, page, pageSize, callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")

	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)

		request.input 'username', sql.VarChar, username
		request.input 'startRow', sql.Int, (page * pageSize - pageSize)
		request.input 'endRow', sql.Int, (page * pageSize - 1)

		request.execute 'districtDaily-getContentByUsername', (err, data) ->
			callback err, data[0]
			return
		return

		)
	return

exports.getContentByUsername = getContentByUsername




makeRecordsList = (day, callback) ->
	moment = require("moment")
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	date = moment(day) #.format('YYYY-MM-DD HH:mm:ss');
	dayBefore = moment(date).subtract("day", 1).set("h", 10).set("m", 0).set("s", 0).format("YYYY-MM-DD HH:mm:ss")
	console.log "makeRecordsList"
	console.log date.toDate()
	console.log dayBefore
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "created", sql.VarChar, date.format("YYYY-MM-DD HH:mm")
		request.verbose = true
		console.log " makeRecordsList ", date, date.toDate()

		# sqlUtil.simpleSql('select * from [dbo].[districtDaily-List] where created >= \'' + dayBefore + '\' and created < \'' + date + '\' order by created asc',
		request.execute "districtDaily-getList", (err, recordset) ->
			recordset = recordset[0]
			if err
				callback err
			else
				records =
					news: []
					vip: []
					garageSale: []
					reject: []

				recordset.forEach (record, k) ->
					console.log "importing content", record.spId, record.title
					bucket = records.news # Default bucket
					if record.tags
						bucket = (if (record.tags.toLowerCase().indexOf("garagesale") + 1) then records.garageSale else bucket)
						bucket = (if (record.tags.toLowerCase().indexOf("vip") + 1) then records.vip else bucket)
						bucket = (if (record.tags.toLowerCase().indexOf("hidefromdd") + 1) then records.reject else bucket)
					bucket = (if (record.title.toLowerCase().indexOf("recall:") + 1) then records.reject else bucket)
					bucket.push record
					return

				callback err, records
			return

		return
	)
	return
exports.makeRecordsList = makeRecordsList





getPost = (post, callback) ->
	sqlUtil = require("../models/sqlUtil")
	sqlUtil.simpleSql "select * from [dbo].[districtDaily-Content] where spId = '" + post + "'", (err, recordset) ->
		callback err, recordset
		return

	return
exports.getPost = getPost





getFocus = (post, callback) ->
	sqlUtil = require("../models/sqlUtil")
	sqlUtil.simpleSql "select * from [dbo].[districtDaily-Scheduled] where id = '" + post + "'", (err, recordset) ->
		callback err, recordset
		return

	return
exports.getFocus = getFocus





getAttachments = (post, callback) ->
	sqlUtil = require("../models/sqlUtil")
	sqlUtil.simpleSql "select path from [dbo].[districtDaily-Attachments] where spId = '" + post + "' order by path ", (err, recordset) ->
		callback err, recordset
		return

	return
exports.getAttachments = getAttachments





getContentFromSharePoint = (callback) ->
	sp = require("../models/sp")
	async = require("async")
	url = "http://daily.kdhcd.org/public/db/db.aspx?mode=getList"
	sp.getSPList url, (err, rows) ->
		if err
			console.error err
			callback err
			false
		else

			#console.log(rows)
			async.each rows, ((row, myCallback) ->
				insertContent row, (err) ->
					myCallback err
					return

				return
			), (err) ->
				callback err
				return

		return

	return
exports.getContentFromSharePoint = getContentFromSharePoint


