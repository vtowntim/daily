exports.day = {
  "weather": {
    "summary": "No precipitation throughout the week, with temperatures falling to 61°F on Tuesday.",
    "days": [
      {
        "time": 1420704000,
        "summary": "Partly cloudy throughout the day.",
        "icon": "partly-cloudy-day",
        "sunriseTime": 1420729818,
        "sunsetTime": 1420765181,
        "moonPhase": 0.61,
        "precipIntensity": 0,
        "precipIntensityMax": 0,
        "precipProbability": 0,
        "temperatureMin": 41.9,
        "temperatureMinTime": 1420725600,
        "temperatureMax": 64.63,
        "temperatureMaxTime": 1420758000,
        "apparentTemperatureMin": 41.9,
        "apparentTemperatureMinTime": 1420725600,
        "apparentTemperatureMax": 64.63,
        "apparentTemperatureMaxTime": 1420758000,
        "dewPoint": 42.54,
        "humidity": 0.7,
        "windSpeed": 0.14,
        "windBearing": 281,
        "visibility": 4.42,
        "cloudCover": 0.52,
        "pressure": 1017.98,
        "ozone": 274.57
      },
      {
        "time": 1420790400,
        "summary": "Mostly cloudy throughout the day.",
        "icon": "partly-cloudy-day",
        "sunriseTime": 1420816214,
        "sunsetTime": 1420851635,
        "moonPhase": 0.64,
        "precipIntensity": 0,
        "precipIntensityMax": 0,
        "precipProbability": 0,
        "temperatureMin": 46,
        "temperatureMinTime": 1420812000,
        "temperatureMax": 65.65,
        "temperatureMaxTime": 1420844400,
        "apparentTemperatureMin": 46,
        "apparentTemperatureMinTime": 1420812000,
        "apparentTemperatureMax": 65.65,
        "apparentTemperatureMaxTime": 1420844400,
        "dewPoint": 42.5,
        "humidity": 0.63,
        "windSpeed": 0.3,
        "windBearing": 37,
        "visibility": 7.98,
        "cloudCover": 0.85,
        "pressure": 1016.08,
        "ozone": 290.95
      },
      {
        "time": 1420876800,
        "summary": "Mostly cloudy throughout the day.",
        "icon": "partly-cloudy-night",
        "sunriseTime": 1420902607,
        "sunsetTime": 1420938090,
        "moonPhase": 0.67,
        "precipIntensity": 0,
        "precipIntensityMax": 0,
        "precipProbability": 0,
        "temperatureMin": 46.38,
        "temperatureMinTime": 1420898400,
        "temperatureMax": 65.55,
        "temperatureMaxTime": 1420930800,
        "apparentTemperatureMin": 44.67,
        "apparentTemperatureMinTime": 1420898400,
        "apparentTemperatureMax": 65.55,
        "apparentTemperatureMaxTime": 1420930800,
        "dewPoint": 38.33,
        "humidity": 0.52,
        "windSpeed": 1.08,
        "windBearing": 136,
        "visibility": 10,
        "cloudCover": 0.56,
        "pressure": 1016.3,
        "ozone": 296.42
      },
      {
        "time": 1420963200,
        "summary": "Mostly cloudy until evening.",
        "icon": "partly-cloudy-day",
        "sunriseTime": 1420988998,
        "sunsetTime": 1421024547,
        "moonPhase": 0.7,
        "precipIntensity": 0,
        "precipIntensityMax": 0,
        "precipProbability": 0,
        "temperatureMin": 46.92,
        "temperatureMinTime": 1420984800,
        "temperatureMax": 64.19,
        "temperatureMaxTime": 1421017200,
        "apparentTemperatureMin": 46.92,
        "apparentTemperatureMinTime": 1420984800,
        "apparentTemperatureMax": 64.19,
        "apparentTemperatureMaxTime": 1421017200,
        "dewPoint": 40.84,
        "humidity": 0.6,
        "windSpeed": 0.4,
        "windBearing": 268,
        "visibility": 10,
        "cloudCover": 0.72,
        "pressure": 1019.01,
        "ozone": 312.41
      },
      {
        "time": 1421049600,
        "summary": "Mostly cloudy throughout the day.",
        "icon": "partly-cloudy-day",
        "sunriseTime": 1421075388,
        "sunsetTime": 1421111004,
        "moonPhase": 0.73,
        "precipIntensity": 0,
        "precipIntensityMax": 0,
        "precipProbability": 0,
        "temperatureMin": 38.19,
        "temperatureMinTime": 1421071200,
        "temperatureMax": 61.37,
        "temperatureMaxTime": 1421107200,
        "apparentTemperatureMin": 38.19,
        "apparentTemperatureMinTime": 1421071200,
        "apparentTemperatureMax": 61.37,
        "apparentTemperatureMaxTime": 1421107200,
        "dewPoint": 38.82,
        "humidity": 0.64,
        "windSpeed": 1.27,
        "windBearing": 257,
        "visibility": 10,
        "cloudCover": 0.64,
        "pressure": 1022.22,
        "ozone": 304.01
      },
      {
        "time": 1421136000,
        "summary": "Partly cloudy in the morning.",
        "icon": "partly-cloudy-day",
        "sunriseTime": 1421161776,
        "sunsetTime": 1421197461,
        "moonPhase": 0.77,
        "precipIntensity": 0,
        "precipIntensityMax": 0,
        "precipProbability": 0,
        "temperatureMin": 37.82,
        "temperatureMinTime": 1421157600,
        "temperatureMax": 60.75,
        "temperatureMaxTime": 1421193600,
        "apparentTemperatureMin": 35.47,
        "apparentTemperatureMinTime": 1421157600,
        "apparentTemperatureMax": 60.75,
        "apparentTemperatureMaxTime": 1421193600,
        "dewPoint": 41.78,
        "humidity": 0.79,
        "windSpeed": 4.13,
        "windBearing": 278,
        "cloudCover": 0.11,
        "pressure": 1023.11,
        "ozone": 282.97
      },
      {
        "time": 1421222400,
        "summary": "Clear throughout the day.",
        "icon": "clear-day",
        "sunriseTime": 1421248161,
        "sunsetTime": 1421283920,
        "moonPhase": 0.8,
        "precipIntensity": 0,
        "precipIntensityMax": 0,
        "precipProbability": 0,
        "temperatureMin": 35.27,
        "temperatureMinTime": 1421244000,
        "temperatureMax": 63.71,
        "temperatureMaxTime": 1421276400,
        "apparentTemperatureMin": 35.27,
        "apparentTemperatureMinTime": 1421244000,
        "apparentTemperatureMax": 63.71,
        "apparentTemperatureMaxTime": 1421276400,
        "dewPoint": 37.07,
        "humidity": 0.66,
        "windSpeed": 2.76,
        "windBearing": 286,
        "cloudCover": 0,
        "pressure": 1024.35,
        "ozone": 282.18
      },
      {
        "time": 1421308800,
        "summary": "Clear throughout the day.",
        "icon": "clear-day",
        "sunriseTime": 1421334545,
        "sunsetTime": 1421370379,
        "moonPhase": 0.83,
        "precipIntensity": 0,
        "precipIntensityMax": 0,
        "precipProbability": 0,
        "temperatureMin": 35.55,
        "temperatureMinTime": 1421330400,
        "temperatureMax": 63.06,
        "temperatureMaxTime": 1421362800,
        "apparentTemperatureMin": 34.35,
        "apparentTemperatureMinTime": 1421334000,
        "apparentTemperatureMax": 63.06,
        "apparentTemperatureMaxTime": 1421362800,
        "dewPoint": 35.88,
        "humidity": 0.64,
        "windSpeed": 2.12,
        "windBearing": 278,
        "cloudCover": 0,
        "pressure": 1023.25,
        "ozone": 292.81
      }
    ]
  },
  "records": {
    "news": [
      {
        "spId": 7501,
        "creator": "Allred, Kathie",
        "title": "Health Literacy Class, January 29th, Blue Room",
        "created": "2015-01-07T21:45:53.000Z",
        "username": "KAllred",
        "tags": null
      },
      {
        "spId": 7502,
        "creator": "DiBernardo, Robert",
        "title": "Mineral King Wing Flooring Project Update",
        "created": "2015-01-07T23:24:54.000Z",
        "username": "rdiberna",
        "tags": "#2"
      }
    ],
    "vip": [],
    "garageSale": [],
    "reject": []
  },
  "focus": {
    "id": 263,
    "date": "2015-01-08T08:00:00.000Z",
    "type": "Quote",
    "subject": "Safety",
    "content": "Safety is the responsibility of all employees.  We are individually accountable for safety and support those who put safety first.",
    "meta": "3",
    "focusID": 176
  },
  "featuredEvents": [
    {
      "id": 2273,
      "spId": "508",
      "creator": "timapp",
      "title": "Test Event",
      "body": "Test abc 1 2 3\n\n{{test-image.png}}",
      "username": null,
      "tags": null,
      "startDate": "2015-02-16T01:00:00.000Z",
      "endDate": "2015-02-16T02:00:00.000Z",
      "allDay": false,
      "recurring": false,
      "location": null,
      "category": "Featured",
      "eventStyle": null,
      "recurrence": null,
      "hits": null
    },
    {
      "id": 2273,
      "spId": "508",
      "creator": "timapp",
      "title": "Second Event",
      "body": "Test abc 1 2 3\n\n",
      "username": null,
      "tags": null,
      "startDate": "2015-02-10T01:00:00.000Z",
      "endDate": "2015-02-10T02:00:00.000Z",
      "allDay": true,
      "recurring": false,
      "location": null,
      "category": "Featured",
      "eventStyle": null,
      "recurrence": null,
      "hits": null
    },
    {
      "id": 2273,
      "spId": "508",
      "creator": "timapp",
      "title": "Third Event",
      "body": "Test abc 1 2 3\n\n",
      "username": null,
      "tags": null,
      "startDate": "2015-02-24T01:00:00.000Z",
      "endDate": "2015-02-24T02:00:00.000Z",
      "allDay": false,
      "recurring": false,
      "location": null,
      "category": "Featured",
      "eventStyle": null,
      "recurrence": null,
      "hits": null
    }
  ],
  "events": {
    "Jan 09 2015": [
      {
        "start": "2015-01-09 18:00",
        "end": "2015-01-09 20:00",
        "record": {
          "id": 2202,
          "spId": "470",
          "creator": "Canafax, Christy",
          "title": "A Legacy of Love",
          "body": "For those looking for support after the loss of a pregnancy or infant up to one year of age.\n\n\r\n \n\n\r\nMeets: Monthly, 2nd Friday\n\n\r\n1100 S. Sowell Ave. Visalia, CA\n\n\r\nInfo: 559-967-0980\n\n",
          "username": null,
          "tags": null,
          "startDate": "2014-11-15T02:00:00.000Z",
          "endDate": "2098-01-11T04:00:00.000Z",
          "allDay": false,
          "recurring": true,
          "location": "Gateway Church Cafe",
          "category": null,
          "eventStyle": null,
          "recurrence": {
            "rule": [
              {
                "firstDayOfWeek": [
                  "su"
                ],
                "repeat": [
                  {
                    "monthlyByDay": [
                      {
                        "$": {
                          "fr": "TRUE",
                          "weekdayOfMonth": "second",
                          "monthFrequency": "1"
                        }
                      }
                    ]
                  }
                ],
                "repeatForever": [
                  "FALSE"
                ]
              }
            ]
          },
          "hits": 101
        },
        "recRule": "monthlyByDay",
        "ruleDetail": {
          "fr": "TRUE",
          "weekdayOfMonth": "second",
          "monthFrequency": "1"
        },
        "occurrence": "2015.9"
      }
    ],
    "Jan 12 2015": [
      {
        "start": "2015-01-12 10:30",
        "end": "2015-01-12 12:00",
        "record": {
          "id": 1750,
          "spId": "290",
          "creator": "Florez, Laura",
          "title": "Loss of Spouse Grief Support Group",
          "body": "Support Group ",
          "username": null,
          "tags": null,
          "startDate": "2014-03-10T17:30:00.000Z",
          "endDate": "2097-05-13T19:00:00.000Z",
          "allDay": false,
          "recurring": true,
          "location": "Quail Park",
          "category": null,
          "eventStyle": null,
          "recurrence": {
            "rule": [
              {
                "firstDayOfWeek": [
                  "su"
                ],
                "repeat": [
                  {
                    "monthlyByDay": [
                      {
                        "$": {
                          "mo": "TRUE",
                          "weekdayOfMonth": "second",
                          "monthFrequency": "1"
                        }
                      }
                    ]
                  }
                ],
                "repeatForever": [
                  "FALSE"
                ]
              }
            ]
          },
          "hits": 79
        },
        "recRule": "monthlyByDay",
        "ruleDetail": {
          "mo": "TRUE",
          "weekdayOfMonth": "second",
          "monthFrequency": "1"
        },
        "occurrence": "2015.12"
      },
      {
        "start": "2015-01-12 12:00",
        "end": "2015-01-12 13:00",
        "record": {
          "id": 1771,
          "spId": "320",
          "creator": "Florez, Laura",
          "title": "Better Breathers",
          "body": "Better Breathers /12:30-1:30PM\n\nMeets: Second Monday of the Month \n\nThe Lifestyle Center\n\n5105 W. Cypress Ave.\n\nInfo: 624-3997\n\n\n\n",
          "username": null,
          "tags": null,
          "startDate": "2014-04-14T19:00:00.000Z",
          "endDate": "2097-06-10T20:00:00.000Z",
          "allDay": false,
          "recurring": true,
          "location": "The Lifestyle Center",
          "category": null,
          "eventStyle": "Educational",
          "recurrence": {
            "rule": [
              {
                "firstDayOfWeek": [
                  "su"
                ],
                "repeat": [
                  {
                    "monthlyByDay": [
                      {
                        "$": {
                          "mo": "TRUE",
                          "weekdayOfMonth": "second",
                          "monthFrequency": "1"
                        }
                      }
                    ]
                  }
                ],
                "repeatForever": [
                  "FALSE"
                ]
              }
            ]
          },
          "hits": 99
        },
        "recRule": "monthlyByDay",
        "ruleDetail": {
          "mo": "TRUE",
          "weekdayOfMonth": "second",
          "monthFrequency": "1"
        },
        "occurrence": "2015.12"
      }
    ],
    "Jan 14 2015": [
      {
        "start": "2015-01-14 18:30",
        "end": "2015-01-14 19:30",
        "record": {
          "id": 2165,
          "spId": "432",
          "creator": "Canafax, Christy",
          "title": "Mended Hearts Support Group",
          "body": "Support Group\n\n",
          "username": null,
          "tags": null,
          "startDate": "2014-10-09T01:30:00.000Z",
          "endDate": "2097-12-12T03:30:00.000Z",
          "allDay": false,
          "recurring": true,
          "location": "Sequoia Regional Cancer Center",
          "category": null,
          "eventStyle": null,
          "recurrence": {
            "rule": [
              {
                "firstDayOfWeek": [
                  "su"
                ],
                "repeat": [
                  {
                    "monthlyByDay": [
                      {
                        "$": {
                          "we": "TRUE",
                          "weekdayOfMonth": "second",
                          "monthFrequency": "1"
                        }
                      }
                    ]
                  }
                ],
                "repeatForever": [
                  "FALSE"
                ]
              }
            ]
          },
          "hits": 24
        },
        "recRule": "monthlyByDay",
        "ruleDetail": {
          "we": "TRUE",
          "weekdayOfMonth": "second",
          "monthFrequency": "1"
        },
        "occurrence": "2015.14"
      },
      {
        "start": "2015-01-14 19:00",
        "end": "2015-01-14 20:00",
        "record": {
          "id": 2099,
          "spId": "372",
          "creator": "Florez, Laura",
          "title": "Valley Renal Support Group",
          "body": "A patient run support group ",
          "username": null,
          "tags": null,
          "startDate": "2014-06-12T02:00:00.000Z",
          "endDate": "2097-08-15T03:00:00.000Z",
          "allDay": false,
          "recurring": true,
          "location": "Kaweah Delta Visalia Dialysis",
          "category": null,
          "eventStyle": "Educational",
          "recurrence": {
            "rule": [
              {
                "firstDayOfWeek": [
                  "su"
                ],
                "repeat": [
                  {
                    "monthlyByDay": [
                      {
                        "$": {
                          "we": "TRUE",
                          "weekdayOfMonth": "second",
                          "monthFrequency": "1"
                        }
                      }
                    ]
                  }
                ],
                "repeatForever": [
                  "FALSE"
                ]
              }
            ]
          },
          "hits": 33
        },
        "recRule": "monthlyByDay",
        "ruleDetail": {
          "we": "TRUE",
          "weekdayOfMonth": "second",
          "monthFrequency": "1"
        },
        "occurrence": "2015.14"
      }
    ]
  },
  "anniversaries": {
    "years": [
      "25",
      "19",
      "14",
      "11",
      "8",
      "7"
    ],
    "data": {
      "7": [
        {
          "id": "lmcgrew",
          "lName": "McGrew",
          "fName": "Ann",
          "dept": "Nursing Admin",
          "username": "lmcgrew",
          "StartDate": "2008-01-08T00:00:00.000Z",
          "years": 7
        }
      ],
      "8": [
        {
          "id": "macervan",
          "lName": "Cervantes",
          "fName": "Martha",
          "dept": "Medical/Surgical- 2S",
          "username": "macervan",
          "StartDate": "2007-01-08T00:00:00.000Z",
          "years": 8
        },
        {
          "id": "lgiesber",
          "lName": "Giesbert",
          "fName": "Lori",
          "dept": "Pediatrics",
          "username": "lgiesber",
          "StartDate": "2007-01-08T00:00:00.000Z",
          "years": 8
        },
        {
          "id": "ajreyes",
          "lName": "Reyes",
          "fName": "Adam",
          "dept": "Central Logistics",
          "username": "ajreyes",
          "StartDate": "2007-01-08T00:00:00.000Z",
          "years": 8
        },
        {
          "id": "hdrichwi",
          "lName": "Richwine",
          "fName": "Heather",
          "dept": "NICU",
          "username": "hdrichwi",
          "StartDate": "2007-01-08T00:00:00.000Z",
          "years": 8
        },
        {
          "id": "lstratto",
          "lName": "Stratton",
          "fName": "Lyman",
          "dept": "Home Health Agency",
          "username": "lstratto",
          "StartDate": "2007-01-08T00:00:00.000Z",
          "years": 8
        }
      ],
      "11": [
        {
          "id": "sbarraga",
          "lName": "Barragan",
          "fName": "Sandy",
          "dept": "Dialysis-Acute",
          "username": "sbarraga",
          "StartDate": "2004-01-08T00:00:00.000Z",
          "years": 11
        },
        {
          "id": "tkgonzal",
          "lName": "Gonzales",
          "fName": "Tiffany",
          "dept": "Admin-Med/Surg Svcs",
          "username": "tkgonzal",
          "StartDate": "2004-01-08T00:00:00.000Z",
          "years": 11
        }
      ],
      "14": [
        {
          "id": "kgilmore",
          "lName": "Gilmore",
          "fName": "Kimberly",
          "dept": "Risk Management",
          "username": "kgilmore",
          "StartDate": "2001-01-08T00:00:00.000Z",
          "years": 14
        },
        {
          "id": "kgress",
          "lName": "Gress",
          "fName": "Kathy",
          "dept": "Broderick Pavilion-Surgical",
          "username": "kgress",
          "StartDate": "2001-01-08T00:00:00.000Z",
          "years": 14
        },
        {
          "id": "klomeli",
          "lName": "Lomeli",
          "fName": "Katrina",
          "dept": "SRCC Medical Oncology Services",
          "username": "klomeli",
          "StartDate": "2001-01-08T00:00:00.000Z",
          "years": 14
        },
        {
          "id": "dmarks",
          "lName": "Marks",
          "fName": "Deborah",
          "dept": "NICU",
          "username": "dmarks",
          "StartDate": "2001-01-08T00:00:00.000Z",
          "years": 14
        }
      ],
      "19": [
        {
          "id": "ccotta",
          "lName": "Cotta",
          "fName": "Charlotte",
          "dept": "Intensive Care Unit",
          "username": "ccotta",
          "StartDate": "1996-01-08T00:00:00.000Z",
          "years": 19
        },
        {
          "id": "sagonzal",
          "lName": "Gonzalez",
          "fName": "Sandra",
          "dept": "Surgical-3N",
          "username": "sagonzal",
          "StartDate": "1996-01-08T00:00:00.000Z",
          "years": 19
        },
        {
          "id": "clewis",
          "lName": "Lewis",
          "fName": "Chrisi",
          "dept": "CVICU",
          "username": "clewis",
          "StartDate": "1996-01-08T00:00:00.000Z",
          "years": 19
        }
      ],
      "25": [
        {
          "id": "rbrown",
          "lName": "Brown",
          "fName": "Rick",
          "dept": "Clinical Information Systems",
          "username": "rbrown",
          "StartDate": "1990-01-08T00:00:00.000Z",
          "years": 25
        }
      ]
    }
  },
  "count": {
    "count": 722
  },
  "recognitions": [
    {
      "fName": null,
      "lName": null,
      "id": null,
      "dept": null,
      "deptID": null
    },
    {
      "fName": null,
      "lName": null,
      "id": null,
      "dept": null,
      "deptID": null
    },
    {
      "fName": "Kristie",
      "lName": "Alvarado",
      "id": "kalvarad",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Meredith",
      "lName": "Alvarado",
      "id": "mealvara",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Rosie",
      "lName": "Arroyo",
      "id": "rarroyo",
      "dept": "Utilization Management",
      "deptID": "8754"
    },
    {
      "fName": "Christian",
      "lName": "Avila",
      "id": "chavila",
      "dept": "Security",
      "deptID": "8420"
    },
    {
      "fName": "Maria",
      "lName": "Basurto",
      "id": "mbasurto",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Alexandra",
      "lName": "Bennett",
      "id": "albennet",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Anthony",
      "lName": "Boxrud",
      "id": "aboxrud",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Sarah",
      "lName": "Brackett",
      "id": "sbracket",
      "dept": "Health Information Management",
      "deptID": "8700"
    },
    {
      "fName": "Mallory",
      "lName": "Breshears",
      "id": "mgorman",
      "dept": "Patient & Family Services",
      "deptID": "8360"
    },
    {
      "fName": "Amanda",
      "lName": "Bridges",
      "id": "ambridge",
      "dept": "Urgent Care",
      "deptID": "7012"
    },
    {
      "fName": "Carly",
      "lName": "Bruton",
      "id": "cbruton",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Darlyn",
      "lName": "Cabral",
      "id": "dcabral",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Christopher",
      "lName": "Calantoc",
      "id": "ccalanto",
      "dept": "Pharmacy",
      "deptID": "8390"
    },
    {
      "fName": "Hector",
      "lName": "Cardenas",
      "id": "hcardena",
      "dept": "Patient Transfer Services",
      "deptID": "6189"
    },
    {
      "fName": "Nourlie",
      "lName": "Carlos",
      "id": "ncarlos",
      "dept": "Rehabilitation",
      "deptID": "6440"
    },
    {
      "fName": "Joel",
      "lName": "Cazares",
      "id": "jcazares",
      "dept": "Sequoia Prompt Care",
      "deptID": "7100"
    },
    {
      "fName": "Trevor",
      "lName": "Cherry",
      "id": "tcherry",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Richard",
      "lName": "Clevenger",
      "id": "rcleveng",
      "dept": "Patient Transfer Services",
      "deptID": "6189"
    },
    {
      "fName": "Annita",
      "lName": "Cline",
      "id": "acline",
      "dept": "Documentation/Pt Throughput",
      "deptID": "8792"
    },
    {
      "fName": "Jaime",
      "lName": "Cortez",
      "id": "jcortez",
      "dept": "Case Management",
      "deptID": "8790"
    },
    {
      "fName": "Cristina",
      "lName": "Custodio",
      "id": "ccustodi",
      "dept": "Oncology-3S",
      "deptID": "6173"
    },
    {
      "fName": "Molly",
      "lName": "DeLaCruz",
      "id": "mdelacru",
      "dept": "Patient & Family Services",
      "deptID": "8360"
    },
    {
      "fName": "Marian",
      "lName": "Derreza",
      "id": "mderreza",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Lauren",
      "lName": "Dodd",
      "id": "ldodd",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Susan",
      "lName": "Dula",
      "id": "sdula",
      "dept": "ICCU-3W",
      "deptID": "6151"
    },
    {
      "fName": "Alfredo",
      "lName": "Estrada",
      "id": "aestrada",
      "dept": "Oncology-3S",
      "deptID": "6173"
    },
    {
      "fName": "Katie",
      "lName": "Farnham",
      "id": "kfarnham",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Fanuel",
      "lName": "Fierros",
      "id": "ffierros",
      "dept": "Patient Transfer Services",
      "deptID": "6189"
    },
    {
      "fName": "Ariel",
      "lName": "Flores",
      "id": "aflores",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Jennifer",
      "lName": "Foster",
      "id": "jfoster",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Melissa",
      "lName": "French",
      "id": "mfrench",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Fernando",
      "lName": "Garcia",
      "id": "fgarcia",
      "dept": "Patient & Family Services",
      "deptID": "8360"
    },
    {
      "fName": "Vanessa",
      "lName": "Garcia-Mendoza",
      "id": "fgarciam",
      "dept": "Customer Service",
      "deptID": "8552"
    },
    {
      "fName": "Janice",
      "lName": "Gearing",
      "id": "jgearing",
      "dept": "Labor-Delivery",
      "deptID": "7400"
    },
    {
      "fName": "Lori",
      "lName": "Giesbert",
      "id": "lgiesber",
      "dept": "Pediatrics",
      "deptID": "6290"
    },
    {
      "fName": "Kandi",
      "lName": "Gist",
      "id": "kgist",
      "dept": "Urgent Care",
      "deptID": "7012"
    },
    {
      "fName": "Maria Elizabeth",
      "lName": "Godinez",
      "id": "mgodinez",
      "dept": "ISS Technical Operations",
      "deptID": "8483"
    },
    {
      "fName": "Sabrina",
      "lName": "Gomez",
      "id": "segomez",
      "dept": "Health Information Management",
      "deptID": "8700"
    },
    {
      "fName": "Yanneth",
      "lName": "Gonzalez-Herrera",
      "id": "yagonzal",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Veronika",
      "lName": "Grier",
      "id": "vgrier",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Hermelinda",
      "lName": "Guzman",
      "id": "hguzman",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Vida",
      "lName": "Halsey",
      "id": "vcooper",
      "dept": "Sequoia Prompt Care on Akers",
      "deptID": "7101"
    },
    {
      "fName": "LaSheena",
      "lName": "Handsbur",
      "id": "lahandsb",
      "dept": "Utilization Management",
      "deptID": "8754"
    },
    {
      "fName": "Katelyn",
      "lName": "Hansen",
      "id": "kmccuske",
      "dept": "NICU",
      "deptID": "6070"
    },
    {
      "fName": "Carey",
      "lName": "Hayes",
      "id": "cpearce",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Lindsey",
      "lName": "Henderson",
      "id": "lhenders",
      "dept": "Medical Telemetry 4T",
      "deptID": "6152"
    },
    {
      "fName": "Laurie",
      "lName": "Hensley",
      "id": "lhensley",
      "dept": "Medical Telemetry 4T",
      "deptID": "6152"
    },
    {
      "fName": "Sarah",
      "lName": "Herrera",
      "id": "seherrer",
      "dept": "Utilization Management",
      "deptID": "8754"
    },
    {
      "fName": "Esmeralda",
      "lName": "Hignojoz",
      "id": "ehignojo",
      "dept": "ICCU-3W",
      "deptID": "6151"
    },
    {
      "fName": "Andrea",
      "lName": "Hodgkins",
      "id": "asoria",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Issa Michelle",
      "lName": "Hurtado",
      "id": "ihurtado",
      "dept": "Rehabilitation",
      "deptID": "6440"
    },
    {
      "fName": "Tim",
      "lName": "Irving",
      "id": "tirving",
      "dept": "Surgery",
      "deptID": "7420"
    },
    {
      "fName": "Susan",
      "lName": "Jackson",
      "id": "sujackso",
      "dept": "Medical Telemetry 4T",
      "deptID": "6152"
    },
    {
      "fName": "Melian",
      "lName": "Johnson",
      "id": "mjohnson",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Amandeep",
      "lName": "Kaur",
      "id": "amandkau",
      "dept": "Orthopedics/Surgery-4S",
      "deptID": "6177"
    },
    {
      "fName": "Rupinder",
      "lName": "Kaur",
      "id": "rukaur",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Sheryl",
      "lName": "Kelly",
      "id": "skelly",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Mateo",
      "lName": "Lambarena",
      "id": "mlambare",
      "dept": "Urgent Care",
      "deptID": "7012"
    },
    {
      "fName": "Jennifer",
      "lName": "Lawrence",
      "id": "jelawren",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Melissa",
      "lName": "Leach",
      "id": "mleach",
      "dept": "Pharmacy",
      "deptID": "8390"
    },
    {
      "fName": "Isabel",
      "lName": "Ledesma",
      "id": "iledesma",
      "dept": "Health Information Management",
      "deptID": "8700"
    },
    {
      "fName": "Lina",
      "lName": "Lewis",
      "id": "lilewis",
      "dept": "Orthopedics/Surgery-4S",
      "deptID": "6177"
    },
    {
      "fName": "Manuel",
      "lName": "Linarez",
      "id": "mlinarez",
      "dept": "Patient Transfer Services",
      "deptID": "6189"
    },
    {
      "fName": "Carly",
      "lName": "Luna",
      "id": "caluna",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Patricia",
      "lName": "Lyons",
      "id": "plyons",
      "dept": "Case Management",
      "deptID": "8790"
    },
    {
      "fName": "Melissa",
      "lName": "Madrid",
      "id": "mmadrid",
      "dept": "ICCU-3W",
      "deptID": "6151"
    },
    {
      "fName": "Patricia",
      "lName": "Maldonado",
      "id": "pmaldona",
      "dept": "Nursery",
      "deptID": "6530"
    },
    {
      "fName": "Kendall",
      "lName": "Mancebo",
      "id": "kmancebo",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Maria",
      "lName": "Marquez",
      "id": "mamarque",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Tommy",
      "lName": "Martinez Jr",
      "id": "tmartine",
      "dept": "Security",
      "deptID": "8420"
    },
    {
      "fName": "Tiffanie",
      "lName": "McWhorter",
      "id": "tmcwhort",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Ann",
      "lName": "Montalvo",
      "id": "amontalv",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Michael",
      "lName": "Munoz",
      "id": "mimunoz",
      "dept": "Patient Transfer Services",
      "deptID": "6189"
    },
    {
      "fName": "Trisha",
      "lName": "Nakasone",
      "id": "ttom",
      "dept": "Pharmacy",
      "deptID": "8390"
    },
    {
      "fName": "Albert",
      "lName": "Navarro",
      "id": "alnavarr",
      "dept": "Pharmacy",
      "deptID": "8390"
    },
    {
      "fName": "Kathleen",
      "lName": "Oliver",
      "id": "koliver",
      "dept": "Health Information Management",
      "deptID": "8700"
    },
    {
      "fName": "Maria",
      "lName": "Orosco",
      "id": "morosco",
      "dept": "Health Information Management",
      "deptID": "8700"
    },
    {
      "fName": "Crystal",
      "lName": "Ortiz",
      "id": "crortiz",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Maria",
      "lName": "Pacheco",
      "id": "mapachec",
      "dept": "Food & Nutrition Svcs-West",
      "deptID": "8341"
    },
    {
      "fName": "Jessica",
      "lName": "Parker",
      "id": "jeparker",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Sandra",
      "lName": "Pillai",
      "id": "sarbuckl",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Viviana",
      "lName": "Quezada",
      "id": "vquezada",
      "dept": "Rehabilitation",
      "deptID": "6440"
    },
    {
      "fName": "Melissa L.",
      "lName": "Quinonez",
      "id": "mquinone",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Leticia",
      "lName": "Ramos",
      "id": "lramos",
      "dept": "Surgical-3N",
      "deptID": "6172"
    },
    {
      "fName": "Amy",
      "lName": "Redwine",
      "id": "aredwine",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Nancy",
      "lName": "Reichert",
      "id": "nreicher",
      "dept": "Labor Triage",
      "deptID": "7235"
    },
    {
      "fName": "Lisa",
      "lName": "Reid",
      "id": "lreid",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Martha",
      "lName": "Renteria",
      "id": "marenter",
      "dept": "Food & Nutrition Svcs-South",
      "deptID": "8345"
    },
    {
      "fName": "Suzette",
      "lName": "Rivera",
      "id": "srivera",
      "dept": "Rehabilitation",
      "deptID": "6440"
    },
    {
      "fName": "Janelle",
      "lName": "Rodriguez",
      "id": "marirodri",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Nancy",
      "lName": "Rodriguez",
      "id": "narodrig",
      "dept": "Food & Nutrition Svcs-South",
      "deptID": "8345"
    },
    {
      "fName": "Mallory",
      "lName": "Rolin",
      "id": "mrolin",
      "dept": "ICCU-3W",
      "deptID": "6151"
    },
    {
      "fName": "Doreen",
      "lName": "Rubio",
      "id": "drubio",
      "dept": "Patient & Family Services",
      "deptID": "8360"
    },
    {
      "fName": "Nikki",
      "lName": "Rudy",
      "id": "nrudy",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Jessica",
      "lName": "Ruiz",
      "id": "jruiz",
      "dept": "Admin-Med/Surg Svcs",
      "deptID": "8726"
    },
    {
      "fName": "Roberta",
      "lName": "Ruiz",
      "id": "rruiz",
      "dept": "Health Information Management",
      "deptID": "8700"
    },
    {
      "fName": "Dorcas",
      "lName": "Saesee",
      "id": "dsaesee",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Julissa",
      "lName": "Sanchez",
      "id": "jrsanche",
      "dept": "Surgical-3N",
      "deptID": "6172"
    },
    {
      "fName": "Mary Ann",
      "lName": "Sanchez-Espinosa",
      "id": "marsanch",
      "dept": "Health Information Management",
      "deptID": "8700"
    },
    {
      "fName": "Alisha",
      "lName": "Sandidge",
      "id": "asandidg",
      "dept": "Orthopedics/Surgery-4S",
      "deptID": "6177"
    },
    {
      "fName": "Ellason",
      "lName": "Schales",
      "id": "ebettenc",
      "dept": "Nursing Float Pool",
      "deptID": "6170"
    },
    {
      "fName": "Michelle",
      "lName": "Schelich",
      "id": "mjacobo",
      "dept": "NICU",
      "deptID": "6070"
    },
    {
      "fName": "Esung",
      "lName": "See",
      "id": "esusee",
      "dept": "Orthopedics/Surgery-4S",
      "deptID": "6177"
    },
    {
      "fName": "Debbie",
      "lName": "Seeger",
      "id": "dseeger",
      "dept": "Nursery Nook-Lactation Store",
      "deptID": "9550"
    },
    {
      "fName": "Jackie",
      "lName": "Servin",
      "id": "jservin",
      "dept": "ICCU-3W",
      "deptID": "6151"
    },
    {
      "fName": "EeSaeng",
      "lName": "Sing",
      "id": "ssing",
      "dept": "Health Information Management",
      "deptID": "8700"
    },
    {
      "fName": "Laura",
      "lName": "Solis",
      "id": "lsolis",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Melissa",
      "lName": "Sparshott",
      "id": "mpeterso",
      "dept": "Emergency Department",
      "deptID": "7010"
    },
    {
      "fName": "Shauna",
      "lName": "Standridge",
      "id": "sdmartin",
      "dept": "Patient Access",
      "deptID": "8560"
    },
    {
      "fName": "Catherine",
      "lName": "Stevens",
      "id": "cstevens",
      "dept": "Surgery",
      "deptID": "7420"
    },
    {
      "fName": "Mona",
      "lName": "Stoner",
      "id": "mstoner",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Juliet",
      "lName": "Tapia",
      "id": "jtapia",
      "dept": "Rehabilitation",
      "deptID": "6440"
    },
    {
      "fName": "Jaymie",
      "lName": "Thompson",
      "id": "jathomps",
      "dept": "Medical Telemetry 4T",
      "deptID": "6152"
    },
    {
      "fName": "Kimberly",
      "lName": "Thompson",
      "id": "kthompso",
      "dept": "Labor-Delivery",
      "deptID": "7400"
    },
    {
      "fName": "Kelly",
      "lName": "Tomlinson",
      "id": "ktomlins",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Liz",
      "lName": "Torres",
      "id": "litorres",
      "dept": "Acute Psych",
      "deptID": "6341"
    },
    {
      "fName": "Margaret",
      "lName": "Torres",
      "id": "mtorres",
      "dept": "Rehabilitation",
      "deptID": "6440"
    },
    {
      "fName": "Virginia",
      "lName": "Turner",
      "id": "vturner",
      "dept": "ICCU-3W",
      "deptID": "6151"
    },
    {
      "fName": "Jaime",
      "lName": "Tuttle",
      "id": "jtuttle",
      "dept": "Medical/Surgical- 2S",
      "deptID": "6181"
    },
    {
      "fName": "Johanna",
      "lName": "Velazquez",
      "id": "jvelazqu",
      "dept": "Orthopedics/Surgery-4S",
      "deptID": "6177"
    },
    {
      "fName": "Zachary",
      "lName": "Vernon",
      "id": "zvernon",
      "dept": "Patient Transfer Services",
      "deptID": "6189"
    },
    {
      "fName": "Gabriela",
      "lName": "Villalta",
      "id": "gvillalt",
      "dept": "Cardiac Services-2N",
      "deptID": "6150"
    },
    {
      "fName": "Goldie",
      "lName": "Walter",
      "id": "gwalter",
      "dept": "Environmental Svcs-Main Campus",
      "deptID": "8440"
    },
    {
      "fName": "Joseph",
      "lName": "Williams",
      "id": "jowillia",
      "dept": "Patient Transfer Services",
      "deptID": "6189"
    }
  ],
  "listDay": "2015-01-08 10:00",
  "title": "District Daily for January 8th, 2015",
  "pageType": "list"
}