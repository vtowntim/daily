var Forecast = require('forecast');

// Initialize
var forecast = new Forecast({
  service: 'forecast.io',
  key: '779e13669edc89137c1f695999abdaa5',
  units: 'us', // Only the first letter is parsed
  cache: true,      // Cache API requests?
  ttl: {           // How long to cache requests. Uses syntax from moment.js: http://momentjs.com/docs/#/durations/creating/
      minutes: 27,
      seconds: 45
    }
});



var getForecast = function(callback){
	// Retrieve weather information from coordinates
	forecast.get([36.3242, -119.3072], function(err, weather){

		var myForcast = {
			summary: weather.daily.summary,
			days: weather.daily.data
		};

		callback(err, myForcast, weather);

	});
}
exports.getForecast = getForecast;