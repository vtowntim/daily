newPathName = (path) ->
	path.replace /(.*\/Attachments\/)(.*)\/(.*)/, "$2_$3"

exports.newPathName = newPathName




getClientAddress = (req) ->
	(req.headers["x-forwarded-for"] or "").split(",")[0].split(":")[0] or req.connection.remoteAddress.split(":")[0]

exports.getClientAddress = getClientAddress




encrypt = (text) ->
	console.log text
	crypto = require("crypto")
	cipher = crypto.createCipher("aes-256-cbc", "d6F3Efeq")
	text = JSON.stringify(text)
	crypted = cipher.update(text, "utf8", "hex")
	crypted += cipher.final("hex")
	crypted

exports.encrypt = encrypt




decrypt = (text) ->
	return null  unless text
	crypto = require("crypto")
	decipher = crypto.createDecipher("aes-256-cbc", "d6F3Efeq")
	dec = decipher.update(text, "hex", "utf8")
	dec += decipher.final("utf8")
	dec = JSON.parse(dec)  if dec.substring(0, 1) is "\"" or dec.substring(0, 1) is "{" or dec.substring(0, 1) is "["
	dec

exports.decrypt = decrypt




checkForLogin = (req, res, successCallback) ->

	if req.query.u # if the username is passed in the URL
		user = decrypt(req.query.u)
		res.cookie "user", user
		res.clearCookie "userData"
		redirectionURL = req._parsedUrl.pathname + '/'
		redirectionURL += req.params.employee  if req.params.employee
		res.redirect redirectionURL
		return false
	if req.cookies.user and not req.cookies.userData # if a cookie has userdata and the URL doesnt have userdata
		kdPeople = require("../models/kdPeople")
		kdPeople.getUser req.cookies.user, (err, userData) ->
			# console.log 'utils> userData', userData
			res.cookie "userData", encrypt(userData)
			successCallback userData
			return

	else
		successCallback decrypt(req.cookies.userData) # try to decrypt the cookie
	return

exports.checkForLogin = checkForLogin




cleanHTML = (dirtyHTML, superStrict) ->
	sanitizeHtml = require("sanitize-html")
	return null  unless dirtyHTML
	dirtyHTML.replace /&quot;/g, "\""
	options =
		allowedAttributes:
			a: [
				"href"
				"style"
			]
			p: [
				"stlye"
				"class"
			]
			img: [
				"src"
				"style"
			]
			span: ["style"]
			div: ["style"]

		allowedTags: sanitizeHtml.defaults.allowedTags.concat(["img"])
		transformTags:
			body: sanitizeHtml.simpleTransform("div",
				class: "body"
			)

		selfClosing: []

	if superStrict
		options.allowedTags = [
			"div"
			"br"
			"p"
		]
	clean = sanitizeHtml(dirtyHTML, options)
	if superStrict
		clean.replace(/<\/(div|p)>/g, "\n\n").replace(/<\/?br(\s?\/)?>/g, "\n").replace(/<(div|p)>/g, "").replace /\&nbsp;/g, " "
	else
		clean.replace(/\>(\d\.)\W+/g, "><span class=\"number\">$1</span>").replace(/\_{5,}/g, "<hr/>").replace(/\&nbsp;/g, " ").replace(/(<br><\/br>)/g, "<br/>").replace(/href="\//g, "href=\"http://kdcentral.kdhcd.org/").replace(/\/Lists\/KDNow\/Attachments\/(\d*)\/([\w\s\%\&\.\?\=\-]*)/g, "/public/attachments/$1_$2").replace(/\·\W{1,}/g, "<span class=\"bullet\">·</span>").replace /([\r\n]){3,}/g, "$1"

exports.cleanHTML = cleanHTML



