async = require("async")
moment = require("moment")
sp = require("../models/sp")
sql = require("mssql")
sqlUtil = require("../models/sqlUtil")
us = require("underscore")._
utils = require("../models/utils")
xml2js = require("xml2js").parseString
noonToday = moment().set("h", 0).set("m", 0).set("s", 0)
startOfMonth = moment(noonToday).startOf("month")
nextWeek = moment().set("h", 23).set("m", 59).set("s", 0).add("w", 1)

spWeekdayMap =
	su: 0
	mo: 1
	tu: 2
	we: 3
	th: 4
	fr: 5
	sa: 6

stMap =
	first: 1
	second: 2
	third: 3
	fourth: 4
	fifth: 5
	last: -1






getFeaturedEvents = (callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.query "select * from [dbo].[districtDaily-Events] where [category] like '%featured%' order by [startDate]", (err, recordset) ->
			callback err, recordset
			return
		)
	return
exports.getFeaturedEvents = getFeaturedEvents






getEventsList = (callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "startDate", sql.VarChar, noonToday.format("YYYY-MM-DD HH:mm:ss")
		
		unless callback
			callback = ->
				return

		request.execute "districtDaily-getEventWeek", (err, recordset) ->
			recordset = recordset[0]
			
			#Expand the JSON-encoded recurrence field:
			recordset.forEach (record, k) ->
				record.recurrence = JSON.parse(record.recurrence)   if record.recurrence?
				return

			callback err, recordset
			return

		return
	)
	return

exports.getEventsList = getEventsList






weekdayOfMonth = (st, weekday, context) -> # first, 4 (thurs), 2014-04-28
	startOfMonth = moment(context).startOf("month")
	unless st is "last"
		adjust = (if startOfMonth.day() <= weekday then -1 else 0)
		adjust += stMap[st]
		adjDate = moment(startOfMonth).day(weekday + (adjust * 7))
	else
		adjDate = moment(context).add("M", 1).date(1).day(weekday - 7)
	
	# console.log('WeekdayOfMonth')
	console.log "weekdayOfMonth", adjDate.format()
	adjDate

meetsMonthFrequency = (context, freq, startDate) ->
	response = (if (moment(context).diff(moment(startDate), "months") % freq) then false else true)
	
	# console.log('Meets Month Frequency?');
	# console.log(context.toDate());
	# console.log(freq);
	# console.log(startDate);
	# console.log(response);
	response

isInDateRange = (context, startDate, endDate) ->
	response = (if (moment(context).isAfter(startDate) and moment(context).isBefore(endDate)) then true else false)
	
	# console.log('Is In Date Range?');
	# console.log(context.toDate());
	# console.log(startDate.toDate());
	# console.log(endDate.toDate());
	
	# console.log(response);
	response

evaluateRecurrence = (currentRecord, dateWindowFrom, dateWindowTo) ->
	
	# console.log('FINDING RECURRENCES');
	# console.log( JSON.stringify(currentRecord) );
	startTime = moment(currentRecord.record.startDate).format("HH:mm")
	endTime = moment(currentRecord.record.endDate).format("HH:mm")
	repeat = currentRecord.record.recurrence.rule[0].repeat[0]
	currentRecord.recRule = us.keys(repeat)[0]
	currentRecord.ruleDetail = repeat[currentRecord.recRule][0]["$"]
	ruleWeekDay = spWeekdayMap[us.keys(currentRecord.ruleDetail)[0]]
	windowEnd = (if currentRecord.record.recurrence.rule[0].windowEnd then currentRecord.record.recurrence.rule[0].windowEnd[0] else currentRecord.record.endDate)
	evaluatedRecordsArray = []
	switch currentRecord.recRule
		when "monthlyByDay"
			console.log "weekdayOfMonth"
			console.log "record", currentRecord.record.title
			console.log currentRecord.ruleDetail.weekdayOfMonth
			console.log "ruleWeekDay"
			console.log ruleWeekDay
			
			# var startTime = {
			#     hour: moment(currentRecord.record.startDate).hour()
			#     minute: moment(currentRecord.record.startDate).minute()
			# }
			
			# var endTime = {
			#     hour: moment(currentRecord.record.endDate).hour()
			#     minute: moment(currentRecord.record.endDate).minute()
			# }
			
			#Get Occurrences for this month and next month... -- for example, thisMonth == the second thursday of this month -- nextMonth == the second thursday of next month
			thisMonth = weekdayOfMonth(currentRecord.ruleDetail.weekdayOfMonth, ruleWeekDay, dateWindowFrom)
			nextMonth = weekdayOfMonth(currentRecord.ruleDetail.weekdayOfMonth, ruleWeekDay, moment(dateWindowFrom).add("month", 1))
			
			# var thisMonth = moment(thisMonth.format('YYYY-MM-DD ') + startTime);
			# var nextMonth = moment(nextMonth.format('YYYY-MM-DD ') + startTime);
			console.log "thisMonth", thisMonth.format()
			console.log "nextMonth", nextMonth.format()
			if isInDateRange(thisMonth, dateWindowFrom, dateWindowTo) and meetsMonthFrequency(thisMonth, currentRecord.ruleDetail.monthFrequency, currentRecord.record.startDate)
				
				# Is this month's occurrence within the date range? (the next two weeks) ..and does it meet the monthFrequency?
				currentRecord.start = thisMonth.format("YYYY-MM-DD ") + startTime
				currentRecord.end = thisMonth.format("YYYY-MM-DD ") + endTime
				evaluatedRecordsArray.push us.clone(currentRecord)
			else if isInDateRange(nextMonth, dateWindowFrom, dateWindowTo) and meetsMonthFrequency(nextMonth, currentRecord.ruleDetail.monthFrequency, currentRecord.record.startDate)
				
				# Is next month's occurrence within the date range? (the next two weeks) ..and does it meet the monthFrequency?
				currentRecord.start = nextMonth.format("YYYY-MM-DD ") + startTime
				currentRecord.end = nextMonth.format("YYYY-MM-DD ") + endTime
				evaluatedRecordsArray.push us.clone(currentRecord)
		when "weekly"
			adjust = (if moment(dateWindowFrom).day() < ruleWeekDay then -1 else 0)
			dayFormulas = [

			]
			
			# console.log('record', currentRecord.record.title)
			# console.log('dateWindowFrom', dateWindowFrom.format())
			# console.log('dayFormulas', dayFormulas)
			dayFormulas.forEach (dayFormula, index) ->
				day = moment(dateWindowFrom).day(dayFormula)
				if isInDateRange(day, dateWindowFrom, dateWindowTo)
					currentRecord.start = day.format("YYYY-MM-DD ") + startTime
					currentRecord.end = day.format("YYYY-MM-DD ") + endTime
					evaluatedRecordsArray.push us.clone(currentRecord)
				return

		when "daily"
			laterStartDate = (if moment(dateWindowFrom).isAfter(currentRecord.start) then moment(dateWindowFrom) else moment(currentRecord.start))
			soonerEndDate = (if moment(windowEnd).isBefore(dateWindowTo) then moment(windowEnd) else moment(dateWindowTo))
			i = 0

			while moment(laterStartDate).add(i, "days").isBefore(soonerEndDate)
				day = moment(laterStartDate).add(i, "days")
				
				# console.log(day.format());
				if isInDateRange(day, dateWindowFrom, dateWindowTo)
					currentRecord.start = day.format("YYYY-MM-DD ") + startTime
					currentRecord.end = day.format("YYYY-MM-DD ") + endTime
					evaluatedRecordsArray.push us.clone(currentRecord)
				i++
	
	# console.log( evaluatedRecordsArray )
	evaluatedRecordsArray

exports.evaluateRecurrence = evaluateRecurrence






evaluateMultiDayOccurrance = (record, dateWindowFrom, dateWindowTo) ->
	[]

makeOccurrence = (record, dateWindowFrom, dateWindowTo) ->
	
	# console.log('making occurrence');
	# console.log(record);
	startDate = moment(record.startDate)
	endDate = moment(record.endDate) # turn the start and end dates to moments()
	endDate = (if endDate.isBefore(dateWindowTo) then endDate else dateWindowTo)
	
	# cr == Current Record
	currentRecord =
		start: record.startDate
		end: record.endDate
		record: record

	occurrenceArray = []
	unless record.recurrence
		unless startDate.isSame(endDate, "day")
			occurrenceArray = occurrenceArray.concat(evaluateMultiDayOccurrance(currentRecord, dateWindowFrom, dateWindowTo))
		else
			occurrenceArray.push us.clone(currentRecord)
	else
		occurrenceArray = occurrenceArray.concat(evaluateRecurrence(currentRecord, dateWindowFrom, dateWindowTo))
	occurrenceArray.forEach (record, k) ->
		record.occurrence = moment(record.start).format("YYYY.DDD")
		return

	occurrenceArray

exports.makeOccurrence = makeOccurrence



makeFeaturedEventsList = (callback) ->
	getFeaturedEvents (err, recordset)->
		recordset = us.filter recordset, (record)-> #Filter out deleted events
			not record.title.match /deleted:/gi
		callback err, recordset
	return
exports.makeFeaturedEventsList = makeFeaturedEventsList





makeEventsList = (callback) ->
	getEventsList (err, recordset) ->
		if err
			callback err
		else
			thisWeek = []
			featured = []

			recordset.forEach (record, k) -> #Iterate thru each returned event
				unless record.title.match /deleted:/gi # If deleted, skip the event
					occurrence = makeOccurrence(record, noonToday, nextWeek) #see if the event has any occurances within the next week
					thisWeek = thisWeek.concat(occurrence) #if there are occurances, add it to the array
				return

			
			# console.log('THIS WEEK ARRAY:');
			# console.log(thisWeek);
			thisWeek.sort (a, b) ->
				return -1   if moment(a.start).isBefore(b.start)
				return 1    if moment(b.start).isBefore(a.start)
				a.record.title - b.record.title

			thisWeekGrouped = {}
			thisWeek.forEach (event, index) ->
				myDay = moment(event.start).format("MMM DD YYYY")
				thisWeekGrouped[myDay] = [] unless thisWeekGrouped[myDay]
				thisWeekGrouped[myDay].push event
				return

			callback err, thisWeekGrouped
		return

	return

exports.makeEventsList = makeEventsList






insertEvent = (row, callback) ->
	return false    unless row
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		
		# recurrence data - @ows_RecurrenceData
		request.input "spId", sql.Int, row["@ows_ID"]
		request.input "body", sql.Text, utils.cleanHTML(row["@ows_Description"], true)
		request.input "creator", sql.VarChar, row["@ows_Author"].match(/\#(.*)/)[1]
		request.input "username", sql.VarChar, null
		request.input "startDate", sql.VarChar, moment(row["@ows_EventDate"]).format("YYYY-MM-DD HH:mm:ss")
		request.input "endDate", sql.VarChar, moment(row["@ows_EndDate"]).format("YYYY-MM-DD HH:mm:ss")
		request.input "title", sql.VarChar, row["@ows_Title"].replace(/\s?\#(\w*)\s?/g, "").replace(/^-\s*|\s$/, "")
		request.input "allDay", sql.Bit, row["@ows_fAllDayEvent"] is "1"
		request.input "recurring", sql.Bit, row["@ows_fRecurrence"] is "1"
		request.input "location", sql.VarChar, row["@ows_Location"]
		request.input "category", sql.VarChar, (if row["@ows_Category"] then row["@ows_Category"] else null)
		request.input "eventStyle", sql.VarChar, (if row["@ows_Event_x0020_Style"] isnt "None Selected" then row["@ows_Event_x0020_Style"] else null)
		tags = row["@ows_Title"].match(/\#(\w*)/g, "")
		tags = (if tags then tags.join() else null)
		request.input "tags", sql.VarChar, tags
		seriesTasks = jsonifyRecurrenceXML: (callback) ->
			unless row["@ows_RecurrenceData"]
				callback null
				return false
			xml2js row["@ows_RecurrenceData"], (err, js) ->
				if err
					callback err
				else
					js = js.recurrence
					request.input "recurrence", sql.Text, JSON.stringify(js)
					callback null
				return

			return

		
		# console.log(request);
		async.series seriesTasks, (err, results) ->
			request.execute "districtDailyInsertEvent", (err, recordsets, returnValue) ->
				console.log "Inserted/Updated EVENT #" + row["@ows_ID"]
				callback err
				return

			return

		return
	)
	return

exports.insertEvent = insertEvent






getEvent = (event, occurrence, callback) ->
	sqlUtil = require("../models/sqlUtil")
	sqlUtil.simpleSql "select top 1 * from [dbo].[districtDaily-Events] where spId = '" + event + "' order by id desc", (err, recordset) ->
		theEvent = recordset[0]
		
		#/TODO: Parse the occurrence parameter to call the makeOccurrence function and return the correct occurence
		callback err, theEvent
		return

	return

exports.getEvent = getEvent






getEventsFromSharePoint = (callback) ->
	url = "http://daily.kdhcd.org/public/db/db.aspx?mode=getEvents"
	sp.getSPList url, (err, rows) ->
		if err
			callback err
			return false
		if rows
			console.log "got event"
			console.log JSON.stringify(rows)
			rows = [rows]   unless Array.isArray(rows)
			async.each rows, ((row, myCallback) ->
				
				# console.log(row); 
				insertEvent row, (err) ->
					myCallback err
					return

				return
			), (err) ->
				callback err
				return

		else
			callback err
		return

	return

exports.getEventsFromSharePoint = getEventsFromSharePoint





