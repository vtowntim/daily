hits = (date, callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	moment = require("moment")
	date = date or new Date()
	console.log "Getting Hits Report For " + moment(date).toDate()
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "reportDate", sql.DateTime, moment(date).toDate()
		request.execute "districtDaily-report-hits", callback
		return
	)
	return
exports.hits = hits



dDailyHits = (date, callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	moment = require("moment")
	date = date or new Date()
	console.log "Getting District Daily Hits Report For " + moment(date).toDate()
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "reportDate", sql.DateTime, moment(date).toDate()
		request.execute "districtDaily-report-hits-Ddaily", callback
		return
	)
	return
exports.dDailyHits = dDailyHits



count = (callback) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	console.log "Getting Count Report"
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.execute "districtDaily-report-count", callback
		return
	)
	return
exports.count = count

makeCSV = (data, filename, res) ->
	_ = require("underscore")

	if data[0].length
		outputFile = []
		outputFile.push _.keys(data[0][0]).join(",") #Add Headers
		data[0].forEach (item) ->
			values = _.values(item)
			values.forEach (elem, index) ->
				console.log elem, typeof elem
				values[index] = elem.replace(/"/g, "\"\"")  if typeof elem is "string"
				return

			outputFile.push "\"" + values.join("\",\"") + "\""
			return

		res.set "Content-Type", "text/csv"
		res.set "Content-disposition", "attachment; filename=#{filename}.csv"
		res.send outputFile.join("\n")
	else
		res.send "No data returned"
	return


recognitions = (req, res, days) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	console.log "Getting Recognitions Report"
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "days", sql.Int, days
		request.execute "districtDaily-report-recognitions", (err, data) ->

			makeCSV data, 'recognitions', res
			return

		return
	)
	return
exports.recognitions = recognitions

transcribedRecognitions = (req, res, days) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	console.log "Getting Recognitions Transcription Report"
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "days", sql.Int, days
		request.execute "districtDaily-report-recognitionsTranscribed", (err, data) ->

			makeCSV data, 'TranscribedRecognitions', res
			return


		return
	)
	return
exports.transcribedRecognitions = transcribedRecognitions



EOM = (req, res, days) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	console.log "Getting EOM Report"
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "days", sql.Int, days
		request.execute "districtDaily-report-EOM", (err, data) ->

			makeCSV data, 'EOM', res
			return

		return
	)
	return
exports.EOM = EOM

transcribedEOM = (req, res, days) ->
	sql = require("mssql")
	sqlUtil = require("../models/sqlUtil")
	console.log "Getting EOM Transcription Report"
	connection = new sql.Connection(sqlUtil.getConfig("DistrictDaily"), (err) ->
		request = new sql.Request(connection)
		request.input "days", sql.Int, days
		request.execute "districtDaily-report-EOMTranscribed", (err, data) ->

			makeCSV data, 'Transcribed_EOM', res
			return


		return
	)
	return
exports.transcribedEOM = transcribedEOM


hitsCSV = (req, res, rundate) ->
	reports = require("../models/reports")
	rundate = rundate or new Date()
	hits rundate, (err, content)->
		res.send content
	return
exports.hitsCSV = hitsCSV


mailHits = (req, res, rundate, sendMail) ->
	reports = require("../models/reports")
	async = require("async")
	districtDaily = require("../models/districtDaily")
	_ = require("underscore")
	moment = require("moment")
	rundate = rundate or new Date()
	weekOf = moment(rundate).startOf("week").add("d", 1).format("MMM D, YYYY")
	processes =
		hits: (callback) ->
			hits rundate, callback
			return

		dDailyHits: (callback) ->
			dDailyHits rundate, callback
			return

	async.parallel processes, (err, content) ->
		content.title = "District Daily Analytics"
		content.subtitle = "For the week of " + weekOf
		content.pageType = "report"
		content.weekOf = weekOf

		# console.log(content.dDailyHits[0][0]);

		# console.log(content.hits[0][0]);
		res.render "mail/report-hits-mail", content, (err, renderedHTML) ->
			unless sendMail
				res.send renderedHTML
			else
				subject = "District Daily Content Ranking Report - Week of " + weekOf

				# var addresses = 'tanderso';
				addresses = _.uniq(_.pluck(content.hits[0][0], "username"))
				addresses.push "DistrictDaily"

				# res.send(renderedHTML);
				districtDaily.blastTheDailyEmail addresses, renderedHTML, res, subject
			return

		return

	return
exports.mailHits = mailHits