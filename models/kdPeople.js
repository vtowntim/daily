
var getUser = function(username, callback){
	var sql = require('mssql');
	var sqlUtil = require('../models/sqlUtil');

	var connection = new sql.Connection(sqlUtil.getConfig('KDPeople'), function(err) {
        var request = new sql.Request(connection);

        username = username == 'MarketingV' ? 'DDesimas' : username

        request.input('username', sql.VarChar, username);

        if(!callback) callback = function(){};

        request.execute('employee.spGetEmployeeByUsername', function(err, userData){
        	// console.log('models/kdpeople > spGetEmployeeByUsername', userData)
            // callback(err, userData[0][0]);
            var ref;

            callback(err, typeof userData !== "undefined" && userData !== null ? (ref = userData[0]) != null ? ref[0] : void 0 : void 0);
        });
    });
}
exports.getUser = getUser;

var searchEmployees = function(searchTerm, callback){
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var connection = new sql.Connection(sqlUtil.getConfig('KDPeople'), function(err) {
        var request = new sql.Request(connection);

        if(!callback) callback = function(){};

        request.input('tim', sql.VarChar, searchTerm);

        request.execute('employee.sp_view_dr_search', callback);
    });
}
exports.searchEmployees = searchEmployees;


var searchPhysicians = function(searchTerm, callback){
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var connection = new sql.Connection(sqlUtil.getConfig('KDPeople'), function(err) {
        var request = new sql.Request(connection);

        if(!callback) callback = function(){};

        request.input('tim', sql.VarChar, searchTerm);

        request.execute('physician.sp_view_dr_search', callback);
    });
}
exports.searchPhysicians = searchPhysicians;


var getEmployeeUsernames = function(callback){
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');
    var us = require("underscore");

    var connection = new sql.Connection(sqlUtil.getConfig('KDPeople'), function(err) {
        var request = new sql.Request(connection);

        if(!callback) callback = function(){};

        request.execute('employee.spDistrictDailyUsernames', function(err, usernames){
            callback(err, us.pluck(usernames[0],'NetworkID') );
        });
    });
}
exports.getEmployeeUsernames = getEmployeeUsernames;

var getAnniversaries = function(date, callback){
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var connection = new sql.Connection(sqlUtil.getConfig('KDPeople'), function(err) {

        var request = new sql.Request(connection);

        if (date){
            var moment = require('moment');
            request.input('date', sql.Date, moment(date).toDate() );
        }

        request.execute('employee.spGetAnniversaries', callback);
    });
}
exports.getAnniversaries = getAnniversaries;
