
var ADsettings = { url: 'ldap://kdhcddc1.KDHCD.org',
	baseDN: 'dc=kdhcddc1,dc=KDHCD,dc=org',
	username: 'timapp',
	password: 'timapp' };


var getGroupUsers = function(req, res){
	var ActiveDirectory = require('activedirectory');
	var groupName = 'downtime';

	var ad = new ActiveDirectory(ADsettings);

	ad.getUsersForGroup(groupName, function(err, users) {
		if (err) { console.error(err); }

		if (! users) res.json('Group: ' + groupName + ' not found.');
		else {
			res.json(JSON.stringify(users));
			}
	});
}
exports.getGroupUsers = getGroupUsers;